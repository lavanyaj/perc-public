//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <CharnyFlowGenerator.h>
#include <assert.h>
#include <vector>
#include <algorithm>
#include <cmath>
#include "ranvar.h"

namespace rcp {

// needs network like xxSim1.ned i.e., one link, two hosts 0 and 1
void CharnyFlowGenerator::initializeCharnySim15() {


    //double endTime;

    //double numRtts = par("endTime").doubleValue();
    //assert(numRtts > 0);
    //endTime = (getLongestRtt() * numRtts);
    //EV << "Last flow starts before " << endTime << "s.\n";

    double numFlows = par("sim15_numSessions").doubleValue();
    assert(numFlows > 0);

    std::string cdfFile = par("sim1x_cdfFile").stringValue();


    EmpiricalRandomVariable erv = EmpiricalRandomVariable(INTER_INTEGRAL);
    erv.loadCDF(cdfFile.c_str());
    double packetSize = header_size_ + payload_size_;
    double meanFlowSize = erv.avg(); // bytes

    double meanFlowSizeBits = meanFlowSize * 8;

    double load = par("sim1x_load").doubleValue();

    //double meanArrivalRate = (load * linkCapacity)/meanFlowSize; // number of flows per second
    assert(linkCapacity.size() > 0);
    double aLinkCapacity = linkCapacity.begin()->second;
    assert(aLinkCapacity > 0); // bps

    double meanInterArrivalTime = (meanFlowSizeBits/(load * aLinkCapacity));
    EV << "Mean flow has " << meanFlowSize << " bytes "\
                << " or " << round(meanFlowSize/packetSize) << " packets "\
                << " or " << prd(round(meanFlowSize/packetSize)/par("ratio").doubleValue(), 2)
                << " RTTs"
                << " and inter-arrival time " << meanInterArrivalTime
                << " seconds "
                << " so " << numFlows << " flows should take "
                << (meanInterArrivalTime  * numFlows)
                << " seconds.\n";

    std::vector<FlowInfo> flows;

    double nextTime = 0;
    double flowId = 0;
    while (flowId < numFlows) {
        double u = uniform(0, 1, 1); //uni(gen); // uniform(minCDF_, maxCDF_, 1);
        double size = erv.value(u); // in bytes

        double numPackets = ceil(size/packetSize); // in packets
        assert(numPackets >= 1);
        double startTime = nextTime; // in seconds
        double e = exponential(meanInterArrivalTime, 2); // use rng 0
        nextTime += e;
        EV << "Random numbers: " << u << ", " << e << ".\n";
        FlowInfo f(flowId, pairInt(0,1), numPackets, par("initialRate"), startTime, -1);
        // will fix flowId again later
        flowId += 1;
        flows.push_back(f);
    }

    assert(flows.size() > 0);
    // one RTT can fit par("ratio").doubleValue() packets

    int i = 0; for (auto& flow: flows) {
        flow.flowId = i++;
    }
    for (auto& f: flows) {
        EV << "Flow " << f.flowId << " starts at " << f.startTime << " and has size " << f.duration\
                << " ( " << prd(f.duration/par("ratio").doubleValue(), 2)<< " RTTs).\n";
        allFlows[f.flowId] = f;
    }
}

} /* namespace rcp */
