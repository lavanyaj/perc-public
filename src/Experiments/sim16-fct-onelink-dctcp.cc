//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <CharnyFlowGenerator.h>
#include <assert.h>
#include <vector>
#include <algorithm>
#include <cmath>
#include <sstream>
#include <fstream>
#include <string>
#include "ranvar.h"

namespace rcp {

// needs network like xxSim1.ned i.e., one link, two hosts 0 and 1
// uses file with list of all actual flows- flow id, size (not CDF)
// used this to run Ideal scheme for the same flows used by ns2 DCTCP simulation
void CharnyFlowGenerator::initializeCharnySim16() {
    //double endTime;

    //double numRtts = par("endTime").doubleValue();
    //assert(numRtts > 0);
    //endTime = (getLongestRtt() * numRtts);
    //EV << "Last flow starts before " << endTime << "s.\n";

    assert(linkCapacity.size() > 0);
    double aLinkCapacity = linkCapacity.begin()->second;
    assert(aLinkCapacity > 0); // bps

    double delayMicroseconds = ((int)(ceil((longestRtt/2)*1e6)));
    // checked that longestRtt is just sum of prop. delays,
    // assuming transmission delay is 0
    std::string flowsFilePrefix = par("cdfFile").stringValue();
    std::stringstream flowsFileName;
    std::cout << "Link capacity " << aLinkCapacity << " which is "
            << (aLinkCapacity/1e9) << "Gbps.\n";
    flowsFileName << flowsFilePrefix << "_cap" << ((int)(aLinkCapacity/1e9)) << "-rtt" << delayMicroseconds
            << "e-6-load" << par("sim1x_load").doubleValue()
            << "-simend3000-connpp1.txt";
    std::cout << "CDF file " << flowsFileName.str();

    //EmpiricalRandomVariable erv = EmpiricalRandomVariable(INTER_INTEGRAL);
    //erv.loadCDF(cdfFile.c_str());
    double packetSize = header_size_ + payload_size_;
    // Checked DCTCP packet size matches up

    std::ifstream flowsFile;
    flowsFile.open(flowsFileName.str(), std::ifstream::in);
    //flowsFile.open("DCTCP_FLOWS_cap10-rtt6e-6-load0.4-simend3000-connpp1.txt");
    if(!flowsFile.is_open()) {
        std::cout << "Error opening " << flowsFileName.str();
        exit(EXIT_FAILURE);
    }
    int lineNum = 0;
    double totalSize = 0;
    int numSizes = 0;

    std::vector<FlowInfo> flows;
    std::string line;
    while(getline(flowsFile, line)) {
        if (lineNum == 0) {
            lineNum++;
            continue;
        }
        lineNum++;
        double numPackets;
        int flowId;
        double startTime;

        std::stringstream ss(line);
        ss >> startTime;
        if (!ss) {
            EV << "Couldn't get start time from " << line;
            continue;
        }
        ss >> flowId;
        if (!ss) {
            EV << "Couldn't get flow id from " << line;
            continue;
        }
        ss >> numPackets;
        if (!ss) {
            EV << "Couldn't get flow id from " << line;
            continue;
        }
        numPackets = (numPackets/1460);
        totalSize += numPackets;
        numSizes += 1;
        FlowInfo f(flowId, pairInt(0,1), numPackets, par("initialRate"), startTime, -1);
        flows.push_back(f);
    }
    flowsFile.close();
    double meanFlowSize = 0;
    if (numSizes > 0) meanFlowSize = (totalSize*packetSize)/numSizes;

    EV << "Mean flow has " << meanFlowSize << " bytes "\
            << " or " << round(meanFlowSize/packetSize) << " packets "\
            << " or " << prd(round(meanFlowSize/packetSize)/par("ratio").doubleValue(), 2) << " RTTs.\n";


    assert(flows.size() > 0);
    // one RTT can fit par("ratio").doubleValue() packets

    for (auto& f: flows) {
        EV << "Flow " << f.flowId << " starts at " << f.startTime << " and has size " << f.duration\
                << " ( " << prd(f.duration/par("ratio").doubleValue(), 2)<< " RTTs).\n";
        allFlows[f.flowId] = f;
    }
}

} /* namespace rcp */
