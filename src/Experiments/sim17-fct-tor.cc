//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <CharnyFlowGenerator.h>
#include <assert.h>
#include <vector>
#include <algorithm>
#include <cmath>
#include "ranvar.h"

namespace rcp {

// needs network like xxSim1.ned i.e., one link, two hosts 0 and 1
void CharnyFlowGenerator::initializeCharnySim17() {


    //double endTime;

    //double numRtts = par("endTime").doubleValue();
    //assert(numRtts > 0);
    //endTime = (getLongestRtt() * numRtts);
    //EV << "Last flow starts before " << endTime << "s.\n";

    double numFlows = par("sim17_numSessions").doubleValue();
    assert(numFlows > 0);

    std::string cdfFile = par("sim1x_cdfFile").stringValue();


    EmpiricalRandomVariable erv = EmpiricalRandomVariable(INTER_INTEGRAL);
    erv.loadCDF(cdfFile.c_str());
    double packetSize = header_size_ + payload_size_;
    double meanFlowSize = erv.avg(); // bytes
    EV << "Mean flow has " << meanFlowSize << " bytes "\
            << " or " << round(meanFlowSize/packetSize) << " packets "\
            << " or " << prd(round(meanFlowSize/packetSize)/par("ratio").doubleValue(), 2) << " RTTs.\n";

    double meanFlowSizeBits = meanFlowSize * 8;

    double load = par("sim1x_load").doubleValue();

    //double meanArrivalRate = (load * linkCapacity)/meanFlowSize; // number of flows per second
    assert(linkCapacity.size() > 0);
    double aLinkCapacity = linkCapacity.begin()->second;
    assert(aLinkCapacity > 0); // bps

    // create a traffic matrix (stores the average rate between each pair of hosts)
//    std::map<pairInt, double> rate_traffic_matrix;
    // create a meanInterArrivalTime traffic matrix (stores the average time between starts of flows for each pair of hosts)
//    std::map<pairInt, double> miat_traffic_matrix;

    double num_hosts = par("sim17_numHosts").doubleValue();

    // for now hard-code the rate_traffic_matrix so that each pair of hosts has equal rate (aLinkCapacity/(num_hosts choose 2))
    // also temporarily hard-code the miat_traffic_matrix so that each host pair has the same interarrival time
    // (Assumes that all of the links have the same capacity)
    double host_pair_maxRate = (aLinkCapacity/(num_hosts-1));
    // with all-to-all traffic there are num_hosts-1 flows
    // arriving at an arbitrary host's ingress link (and
    // ACKs for traffic from the host), one flow/ reverse flow
    // from/ to each of the other hosts. We'll ignore the reverse
    // traffic.
    // We'll assume all hosts are sending at the same rate.
    // Then if every other host is sending at maximum rate
    // to this arbitrary host, then each one gets a share of
    // link_capacity/(num_hosts-1). So the maximum rate that
    // a host pair can send out that's feasible is (aLinkCapacity/(num_hosts-1)).

    double meanInterArrivalTime = (meanFlowSizeBits/(load * host_pair_maxRate));
//    for (int host1=0; host1 < num_hosts; host1++) {
//        for (int host2=0; host2 < num_hosts; host2++) {
//            if (host1 != host2) {
//                rate_traffic_matrix[pairInt(host1, host2)] = host_pair_avgRate;
//                miat_traffic_matrix[pairInt(host1, host2)] = meanInterArrivalTime;
//            }
//        }
//    }

    std::vector<FlowInfo> flows;

    // double nextTime = 0;
    // create a next_time_matrix that keeps track of when flows are to run for each host pair
    // initialize this matrix to all 0's
    std::map<pairInt, double> next_time_matrix;
    for (int host1=0; host1 < num_hosts; host1++) {
        for (int host2=0; host2 < num_hosts; host2++) {
            if (host1 != host2)
                next_time_matrix[pairInt(host1, host2)] = 0;
        }
    }

    // host pairs have iid workload (flow arrival and size)

double flowId = 0;
double num_hostPairs = pow(num_hosts,2) - num_hosts;

for (int host1=0; host1 < num_hosts; host1++) {
        for (int host2=0; host2 < num_hosts; host2++) {
            if (host1 != host2) {

                // distribute the flows evenly amongst the host pairs
                for (int i=0; i < floor(numFlows/num_hostPairs); i++) {
                    // get size of flow
                    double u = uniform(0, 1, 1); //uni(gen); // uniform(minCDF_, maxCDF_, 1);
                    double size = erv.value(u); // in bytes
                    double numPackets = ceil(size/packetSize); // in packets
                    assert(numPackets >= 1);

                    // get start time of flow
                    double e = exponential(meanInterArrivalTime, 2);
                    next_time_matrix[pairInt(host1, host2)] += e;
                    double startTime = next_time_matrix[pairInt(host1, host2)]; // in seconds

                    EV << "Random numbers: " << u << ", " << e << ".\n";

                    FlowInfo f(flowId, pairInt(host1,host2), numPackets, par("initialRate"),
                            startTime, -1);
                    // will fix flowId again later
                    flowId += 1;
                    flows.push_back(f);
                } // for (int i=0; i < numFlows/num_hostPairs; i++)
            } // if (host1 != host2)
        } // for (int host2=0; host2 < num_hosts;
    } // for (int host1=0; host1 < num_hosts; host1++)

    // TOR id is numHosts

    assert(flows.size() > 0);
    // one RTT can fit par("ratio").doubleValue() packets
    int i = 0; for (auto& flow: flows) {
        flow.flowId = i++;
    }
    for (auto& f: flows) {
        EV << "Flow " << f.flowId << " starts at " << f.startTime << " and has size " << f.duration\
                << " ( " << prd(f.duration/par("ratio").doubleValue(), 2)<< " RTTs).\n";
        allFlows[f.flowId] = f;
    }

}


// Computes the number of k length combinations of n
// Careful! - this might overflow if the inputs are too large
int CharnyFlowGenerator::nChoosek( int n, int k )
{
    if (k > n) return 0;
    if (k * 2 > n) k = n-k;
    if (k == 0) return 1;

    int result = n;
    for( int i = 2; i <= k; ++i ) {
        result *= (n-i+1);
        result /= i;
    }
    return result;
}

} /* namespace rcp */
