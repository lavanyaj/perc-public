#include "CharnyFlowGenerator.h"
#include "SwitchRoutingHelper.h"
#include <omnetpp.h>
#include <vector>
#include <sstream>
#include <string>
#include <algorithm>
#include <assert.h>
#include <cstring>

namespace rcp {

Define_Module(CharnyFlowGenerator);
#define EMIT_SIGNAL 0
#define LOG_ACTIVE_FLOWS 0
#define EMIT_FCT_SIGNALS 1

#ifndef EV3
#define EV3 if (false) EV << "EV3: "
#ifndef EV2
#define EV2 if (false) EV << "EV2: "
#ifndef EV1
#define EV1 if (false) std::cout

  // get routing tables and next hop tables
void CharnyFlowGenerator::initializeRoutingAndNextHopTables() {
  for (int src=0; src < topo->getNumNodes(); src++) {
    std::stringstream idSs;
    idSs << "CharnyFlowGenerator Switch " << src;
    std::string idStr(idSs.str());
    assert(nhtablesByPort.count(src) == 0);
    assert(nhtablesByAddr.count(src) == 0);
    getNextHopTable(topo, src, nhtablesByPort[src], nhtablesByAddr[src]);
    assert(rtables.count(src) == 0);
    getRoutingTable(topo, src, nhtablesByAddr.at(src), rtables[src], idStr);
  }  
}

// get paths from topology
void CharnyFlowGenerator::initializeLinkCapacitiesAndDelays() {
  assert(topo != NULL);
  assert(topo->getNumNodes() > 0);

  // get link capacities, routing and next hop tables

  for (int src=0; src<topo->getNumNodes(); src++) {
    assert(topoId.count(topo->getNode(src)->getModuleId()) == 0);
    topoId[topo->getNode(src)->getModuleId()] = src;
  }

  assert(linkCapacity.size() == 0);
  assert(linkDelay.size() == 0);
  // Get link capacities and link delays
  linkCapacity.clear();
  linkDelay.clear();
  for (int src=0; src<topo->getNumNodes(); src++) {
    cTopology::Node* node = topo->getNode(src);
            for (int l= 0; l < node->getNumOutLinks(); l++) {
                cTopology::LinkOut* linkOut = node->getLinkOut(l);
                cTopology::Node* otherNode = linkOut->getRemoteNode();
                cGate* localGate = linkOut->getLocalGate();
                cDatarateChannel *chan = check_and_cast<cDatarateChannel *> \
                        (localGate->getTransmissionChannel());
                double cap = chan->getDatarate();
                double delay = SIMTIME_DBL(chan->getDelay());

		assert(node->getModule()->getIndex() == topoId.at(node->getModuleId()));

                Link link(topoId.at(node->getModuleId()), topoId.at(otherNode->getModuleId()));
		assert(linkCapacity.count(link) == 0);
		assert(linkDelay.count(link) == 0);
                linkCapacity[link] = cap;
                linkDelay[link] = delay;
            }
        }

        for (auto& lc : linkCapacity)
            EV1 << "Link (" << lc.first.first << ", " << lc.first.second << ")"\
            << " has capacity " <<  lc.second << " Bps.\n";
}

  void CharnyFlowGenerator::initializeLongestRtt() {
    assert(topo != NULL);
    assert(topo->getNumNodes() > 0);

   // Get shortest paths to find max delay
    std::map<Ends, std::vector<Link>> sessionPathsByEnds;

    for (int src=0; src<topo->getNumNodes(); src++) {
      topo->calculateUnweightedSingleShortestPathsTo(topo->getNode(src));
      for(int dst=0; dst<topo->getNumNodes(); dst++) {
	std::vector<Link> path;
	if (topo->getNode(dst)->getNumPaths() == 0) continue;
	cTopology::Node *tmp = topo->getNode(dst);
	int length = 0;
	while (tmp != topo->getNode(src) and length < 10) {
	  cTopology::Node *outNode = tmp->getPath(0)->getRemoteNode();
	  path.push_back(Link(topoId[tmp->getModuleId()],		\
			      topoId[outNode->getModuleId()]));
	  tmp = outNode;
	  length++;
	}
	assert(sessionPathsByEnds.count(Ends(dst,src)) == 0);
	sessionPathsByEnds[Ends(dst, src)] = path;
      }
    }
  
    struct MaxDelay {
      Ends ends;
      double value;
      MaxDelay(Ends ends, double value) : ends(ends), value(value) {}
    } maxDelay(Ends(-1, -1), -1);
    std::map<Ends, double> sessionPathsDelay;
    for (auto& sp: sessionPathsByEnds) {
      assert(sp.second.size() > 0);
      double totalDelay = 0;
      for (auto& link : sp.second) {
	double transmissionDelay = 0; //(header_size_*8/linkCapacity.at(link));
	EV1 << "Transmission delay of Link (" << link.first << ", " << link.second\
	    << ") : " << transmissionDelay << " s.\n";
	double propagationDelay = linkDelay.at(link);
	EV1 << "Propagation delay of Link (" << link.first << ", " << link.second\
	    << ") : " << propagationDelay << " s.\n";
	totalDelay += transmissionDelay+propagationDelay;
      }
      if (sessionPathsDelay.count(sp.first) == 0) {
	sessionPathsDelay[sp.first]  = totalDelay;
	if (maxDelay.value == -1 or totalDelay > maxDelay.value)
	  maxDelay = MaxDelay(sp.first, totalDelay);
	EV1 << "Total RTT from " << maxDelay.ends.first << " to "\
	    << maxDelay.ends.second << ": " << 2 * totalDelay << ".\n";
      }

    }
    EV2 << "Longest RTT is for path from " << maxDelay.ends.first << " to "\
	<< maxDelay.ends.second << ": " << 2 * maxDelay.value << " s.\n";
    longestRtt = 2 * maxDelay.value;
  }

void CharnyFlowGenerator::setupSignals() {

    for (const auto& statName : statNames ) {
        signals[statName] = registerSignal(statName.c_str());
    }

  }

// initialize signals, sendFlowMsgs() for self, setup check optimal timeouts
void CharnyFlowGenerator::initialize()
{

    const char *vinit[] = {"waterfilling", "bottleneck", "waterfillingInc", "flowChange",\
            "activeFlows", "flowSynTime", "flowSynId", "flowFirstDataTime",\
            "flowFirstDataId", "flowFinTime", "flowFinId", "flowFinackTime",\
            "flowFinackId", "flowSynSize", "activeFlowsTime", \
            "flowCompletionTime", "optimalWithinXRtts", "optimalTime", "flowStats", "totalSendingRate",\
            "totalSendingRatePerRtt", "flowArrival", "flowArrivalRate", "dupSyn", "dupFin", "dupFinack",\
            "dupFirstData", "flowSynWaterfillingInc", "flowFinWaterfillingInc",\
            "flowSynBottleneck", "flowFinBottleneck", "buggyEvent","flowsHaveConverged_simTime","flowsHaveConverged_realTime"};

    statNames =  std::vector<std::string>(vinit, vinit + sizeof(vinit)/sizeof(char *));

    if (EMIT_SIGNAL || EMIT_FCT_SIGNALS) setupSignals();

    maxFlows = 100; 
    // for all other sims/ toy examples, maxFlows is never more than 100
    
    header_size_ = par("headerSize").doubleValue();
    payload_size_ = par("payloadSize").doubleValue();


    int sim = par("sim");
    EV3 << "Sim number is " << sim << "\n";

    topo = new cTopology("topo");
    
    std::vector<std::string> nedTypes;
    nedTypes.push_back(par("nodeType").stringValue());
    topo->extractByNedTypeName(nedTypes);
    EV3 << "FlowGen cTopology found " << topo->getNumNodes()		\
	<< " "  << par("nodeType").stringValue() << " nodes\n";

    assert(topo->getNumNodes() > 0);

    // TODO(lav): don't need all this either, except to get longestRtt
    initializeLinkCapacitiesAndDelays();
    initializeLongestRtt();
    initializeRoutingAndNextHopTables();

    assert(linkCapacity.size() > 0);

    switch(sim) {
      // Only FCT experiments
    case 1: initializeCharnySim1(); break; // in sim01-intro.cc (hotnets intro example, 1 link and two flows)
    case 15: initializeCharnySim15(); break; // in sim15-fct-onelink.cc (FCT EXPERIMENTS FOR HOTNETS)
    case 16: initializeCharnySim16(); break; // in sim16-fct-onelink-dctcp.cc (FCT EXPERIMENTS TO RUN IDEAL FOR NS2 DCTCP FLOWS FOR HOTNETS)
    case 17: initializeCharnySim17(); break; // in sim17-fct-tor.cc
    default:
        EV << "Sim number not recognized " << sim;
    }

    std::vector<FlowInfo> flows;
    for (auto& af : allFlows) {
        flows.push_back(af.second);
    }
    sendFlowMsgs(flows);
}

void CharnyFlowGenerator::finish() {

    // Delete unsent flow messages
    while (!flowMsgs.empty()) {
        CharnyMakeFlowMsg* nextMsg = flowMsgs.back();
        flowMsgs.pop_back();
        delete nextMsg;
        nextMsg = NULL;
    }

    delete topo;
    topo = NULL;

}

// schedule start/end flows messages for self to fwd
void CharnyFlowGenerator::sendFlowMsgs(std::vector<FlowInfo> flows) {

    flowMsgs.clear();

    for (const auto& f: flows) {
      assert(f.endTime == -1 and f.duration > 0);
      // Make a start flow message
      // For flow completion time experiments, there is no end flow message, only start time (0) and duration.
      // Only used for ordering messages, actual send time set only when active flows after previous
      // messages have stabilized.
      assert(f.startTime >= 0);
      std::stringstream msgName;
      msgName.clear();
      msgName.str("");
      msgName << "start flow # " << f.flowId << " from " << f.ends.first << " to " << f.ends.second << ".\n";

      CharnyMakeFlowMsg* flowMsg = new CharnyMakeFlowMsg(msgName.str().c_str());
      int src = f.ends.first; int dest = f.ends.second;
      flowMsg->setSource(src);
      flowMsg->setDestination(dest);
      flowMsg->setFlowId(f.flowId);
      flowMsg->setChangeType(CHARNY_MAKEFLOW_START);
      flowMsg->setSendTime(f.startTime);
      flowMsg->setDuration(f.duration);
      flowMsg->setRate(0);
      flowMsg->setMinRtt(longestRtt);
      flowMsgs.push_back(flowMsg);

      EV3 << "Scheduling " << flowMsg->getName() << " to send at time " << f.startTime << ".\n";

      EV1 << "Flow # " << f.flowId << " starts at " << f.startTime << " and ends at " << f.endTime << ".\n";
    }

    assert(!flowMsgs.empty());

    // Sort descending, and pop from back..
    std::sort(flowMsgs.begin(), flowMsgs.end(), \
            [](const CharnyMakeFlowMsg* a, const CharnyMakeFlowMsg* b) -> bool
            { return a->getSendTime() > b->getSendTime(); }); // if a's time is bigger return true
    // Schedule the first message from flowMsgs

    scheduleNextFlowMsgs();
}



void CharnyFlowGenerator::handleControl(CharnyMakeFlowMsg* flowMsg) {
    bool valid = true;
    int flowId = flowMsg->getFlowId();
    // just about to send FIN .. so Flow Gen should start seeing effects in next
    // 1-2 min RTTs.
    double time = simTime().dbl();
    std::string rtts = prd(time/(longestRtt), 2);
    switch(flowMsg->getChangeType()) {

        case CHARNY_MAKEFLOW_SYN:
        {
            EV3 << "Flow Gen (" <<  rtts << "RTTs) : " << flowId << " about to send SYN.\n";

            if (flowStats.count(flowId) == 0) {
                flowStats[flowId] = FlowInfo();
                flowStats.at(flowId).flowId = flowId;
                flowStats.at(flowId).duration = flowMsg->getDuration();
            }

            if (flowStats.at(flowId).synTime == -1)
                flowStats.at(flowId).synTime = time;
            else if (EMIT_SIGNAL) emitSignal("dupSyn", flowId);

             flowArrival += flowMsg->getDuration();
             if (EMIT_SIGNAL) emitSignal("flowArrival", flowMsg->getDuration());
            break;
        }
        case CHARNY_MAKEFLOW_FIRST_DATA:
        {
            if (flowStats.at(flowId).startTime == -1)
                flowStats.at(flowId).startTime = time;
            else if (EMIT_SIGNAL) emitSignal("dupFirstData", flowId);
            EV3 << "Flow Gen (" <<  rtts << "RTTs) : " << flowId << " about to send first data.\n";

            break;
        }
        case CHARNY_MAKEFLOW_FIN:
        {
            if (flowStats.at(flowId).finTime == -1)
                flowStats.at(flowId).finTime = time;
            else if (EMIT_SIGNAL) emitSignal("dupFin", flowId);
            EV3 << "Flow Gen (" <<  rtts << "RTTs) : " << flowId << " about to send FIN.\n";

            // number of flows just before removing
            flowStats.at(flowId).numFlowsWhenRemoved = activeFlows.size();
            break;
        }
        case CHARNY_MAKEFLOW_FINACK:
        {
            if (flowStats.at(flowId).endTime == -1)
                flowStats.at(flowId).endTime = time;
            else if (EMIT_SIGNAL) emitSignal("dupFinack", flowId);
            EV3 << "Flow Gen (" <<  rtts << "RTTs) : " << flowId << " just got FINACK.\n";

            bool stopRcpSwitch = false;
            if (par("nodeType").stdstringValue().find("Rcp")  != std::string::npos) {
                // in FCT experiments
                // if we've sent the last pre-scheduled flow (it started) and no active flows
	      assert(!par("waitToConverge").boolValue() and !par("endAfterConverge").boolValue());
	      if (lastSendTime < simTime().dbl() and activeFlows.size() == 0) {
		stopRcpSwitch = true;
                    
	      }

            if (stopRcpSwitch) {
                for (int i = 0; i < topoId.size(); i++) {
                    cMessage* stopMsg = new cMessage("StopSwitch");
                    EV3 << "sending stop messages to RCP switches.\n";
                    send(stopMsg, "gate$o", i);
                }
            }
            }
            const FlowInfo& f = flowStats.at(flowId);


            if (EMIT_SIGNAL || EMIT_FCT_SIGNALS) emitSignal("flowSynId", f.flowId);
            //if (EMIT_SIGNAL) emitSignal("flowSynTime", f.synTime);
            if (EMIT_SIGNAL || EMIT_FCT_SIGNALS) emitSignal("flowSynSize", f.duration);
            //if (EMIT_SIGNAL) emitSignal("flowFirstDataTime", f.startTime);
            //if (EMIT_SIGNAL) emitSignal("flowFinTime", f.finTime);
            //if (EMIT_SIGNAL) emitSignal("flowFinackTime", f.endTime);
            //if (EMIT_SIGNAL) emitSignal("flowSynWaterfillingInc", f.synWaterfillingInc);
            //if (EMIT_SIGNAL) emitSignal("flowFinWaterfillingInc", f.finWaterfillingInc);

            assert(f.numFlowsWhenAdded <= maxFlows+1);
            assert(f.numFlowsWhenRemoved <= maxFlows+1);

            double fct = f.endTime-f.synTime;
            if (f.endTime == -1 or f.synTime == -1)
                fct = -1;
            if (EMIT_SIGNAL || EMIT_FCT_SIGNALS) emitSignal("flowCompletionTime", fct);
            break;
        }
    }

    // update actual rates: FIN means no more packets after this
    if (flowMsg->getChangeType() == CHARNY_MAKEFLOW_FIN) {
        valid &= (activeFlows.count(flowId)> 0);
        if (activeFlows.count(flowId)> 0) {
            actualRate.erase(flowId); // might already have been erased when we sent END msg
            double total = 0;
            for (const auto ar: actualRate) {
                total += ar.second;
            }
            if (EMIT_SIGNAL) emitSignal("totalSendingRate", total);
            finSent[flowId] = simTime().dbl();
        }
      }
      
    // compute optimal rates on SYN or FIN
    // (whoever said they want to start or they want to send last packet)

    if (LOG_ACTIVE_FLOWS) {

    if (flowMsg->getChangeType() == CHARNY_MAKEFLOW_SYN) {
        valid &= (activeFlows.count(flowId) == 0);
        if (valid) {
            activeFlows.insert(std::pair<int, FlowInfo>(flowId, allFlows.at(flowId)));
            if (EMIT_SIGNAL) emitSignal("activeFlows", activeFlows.size());
            if (EMIT_SIGNAL) emitSignal("activeFlowsTime", simTime().dbl());
            EV3 << "Added " << flowId << " to active flows.\n";
            if (activeFlows.size() == maxFlows+1)
               if (EMIT_SIGNAL) emitSignal("flowChange", flowId);
            flowStats.at(flowId).numFlowsWhenAdded = activeFlows.size();
        }

    }

    if (flowMsg->getChangeType() == CHARNY_MAKEFLOW_FIN)
      valid &= (activeFlows.count(flowId)> 0);
    if (valid) {
      assert(!par("idealMode").boolValue());
      if (maxFlows == -1 || activeFlows.size() == maxFlows+1)
	if (EMIT_SIGNAL) emitSignal("flowChange", (-1*flowId));
      assert(activeFlows.count(flowId)> 0);
      activeFlows.erase(flowId);
      if (EMIT_SIGNAL) emitSignal("activeFlows", activeFlows.size());
      if (EMIT_SIGNAL) emitSignal("activeFlowsTime", simTime().dbl());
      EV3 << "Removed " << flowId << " to active flows.\n";
    }
    }
    
    if (!valid) {
        if (EMIT_SIGNAL || EMIT_FCT_SIGNALS) emitSignal("buggyEvent", flowMsg->getFlowId());
    }
}
// every RTT, checks if last received rates are optimal (checkOptimal)
// if start or end flow message from self sends it out
// if rate change message from a data host
// if wanna start or just ended message from data host, does water-filling updates optimal
// and if in ideal mode, sends out optimal rates to hosts
// TODO: update all hosts to send messages on start and FIN-ACK
void CharnyFlowGenerator::handleMessage(cMessage *msg)
{
    // It's a CharnyMakeFlowMsg.
    CharnyMakeFlowMsg *flowMsg = check_and_cast<CharnyMakeFlowMsg *>(msg);
    int flowId = flowMsg->getFlowId();

    EV3 << "Received MakeFlow message of type "\
                << charnyMakeflowType[flowMsg->getChangeType()]\
                << " for flow " << flowId << ".\n";

    // Start or end flow message to be sent to flow source
    if (msg->isSelfMessage() and\
            (flowMsg->getChangeType() == CHARNY_MAKEFLOW_START or\
                        flowMsg->getChangeType() == CHARNY_MAKEFLOW_END)) {
        send(flowMsg, "gate$o", flowMsg->getSource());
        return;
    }

    assert(!msg->isSelfMessage());
    if ((flowMsg->getChangeType() == CHARNY_MAKEFLOW_SYN\
                       or flowMsg->getChangeType() == CHARNY_MAKEFLOW_FIRST_DATA\
                       or flowMsg->getChangeType() == CHARNY_MAKEFLOW_FIN\
                       or flowMsg->getChangeType() == CHARNY_MAKEFLOW_FINACK)) {
        // Control message receive from end host.
        // End host sends a copy of SYN/ FIN/ FINACK.
        // Also sends a FIRST_DATA msg when it sends it first data packet.
        handleControl(flowMsg);
        if (!flowMsg->isScheduled()) delete flowMsg;
        return;
    }

    if ((flowMsg->getChangeType() == CHARNY_MAKEFLOW_CHANGE_RATE)) {
      if (!flowMsg->isScheduled()) delete flowMsg;
      // ignore and
      return;
    }
    EV3 << "FlowGenerator received weird message of type " << flowMsg->getChangeType();
    assert(false);

}


void CharnyFlowGenerator::emitSignal(std::string name, double value) {
    emit(signals.at(name), value);
    EV << "At time "<< simTime() << ", emitting " << name << ": " << value << "\n";
}

void CharnyFlowGenerator::emitSignal(std::string name, const char* value) {
    emit(signals.at(name), value);
    EV << "At time "<< simTime() << ", emitting " << name << ": " << value << "\n";
}


void CharnyFlowGenerator::scheduleNextFlowMsgs() {
    if (flowMsgs.empty()) return;
    EV3 << "Scheduling all messages at given time.\n";
    while(!flowMsgs.empty()) {
      CharnyMakeFlowMsg* nextMsg = flowMsgs.back();
      flowMsgs.pop_back();
      scheduleAt(nextMsg->getSendTime(), nextMsg);
      
      std::stringstream ss;
      ss.clear(); ss.str("");
      ss << nextMsg->getSendTime() << ": " << charnyMakeflowType[nextMsg->getChangeType()] << " " \
	 << nextMsg->getFlowId() << ": " << nextMsg->getName();
      EV3 << "At time " << prd(SIMTIME_DBL(simTime())/(longestRtt), 2) << \
	" RTTs scheduling " << ss.str() << " (" << flowMsgs.size() << " flow messages left.)\n";
      lastSendTime = nextMsg->getSendTime();
    }
}

void orderFlowsByStartTime(std::vector<FlowInfo>& flows) {
    std::sort(flows.begin(), flows.end(),\
            [](const FlowInfo& a, const FlowInfo& b) -> bool
            { return a.startTime > b.startTime; });
    int i = 0; for (auto& flow: flows) flow.flowId = i++;
    return;
}




#endif // EV1
#endif // EV2
#endif // EV3

Define_Module(SimpleFlowGenerator);

void SimpleFlowGenerator::initialize()
{
    scheduleAt(simTime(), getGenericPacket());
}

void SimpleFlowGenerator::handleMessage(cMessage *msg) {
    if (!std::strcmp(msg->getName(), "generic")) {
        GenericPacket *flowMsg = check_and_cast<GenericPacket *>(msg);
        if (msg->isSelfMessage()) {
            send(flowMsg, "gate$o", flowMsg->getSource());
            scheduleAt(simTime(), getGenericPacket());
            return;
        }
    }
}

GenericPacket* SimpleFlowGenerator::getGenericPacket() {
    GenericPacket* flowMsg = new GenericPacket();
    flowMsg->setSource(0);
    flowMsg->setDestination(0);
    flowMsg->setPktType(0);
    flowMsg->setFlowId(0);
    return flowMsg;
}

void SimpleFlowGenerator::finish() {
    return;
}
}; // namespace

//python -c 'import fileinput; time = [0]; map(lambda line: time.append(float(line.split()[3])-time[-1]), fileinput.input()); print time'
