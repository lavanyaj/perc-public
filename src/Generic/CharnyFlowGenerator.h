//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __RCP_CHARNYFLOWGENERATOR_H
#define __RCP_CHARNYFLOWGENERATOR_H

#include <omnetpp.h>
#include "util.h"
#include <time.h>
typedef int Session;


namespace rcp {

class CharnyFlowGenerator : public cSimpleModule
{
    private:
        std::map<std::string, simsignal_t> signals;

        double header_size_;
        double payload_size_;

        int maxFlows;
        double flowArrival;

        double longestRtt;
        std::map<Link, double> linkCapacity;
        std::map<Link, double> linkDelay;
        std::map<int, int> topoId;
        std::map<int, double> actualRate;
        std::map<int, double> finSent;

	double lastSendTime;

	// set of unique paths
	std::map<std::string, int> uniquePaths;
	// map from path Id to path
	std::map<int, std::vector<Link> > sessionPaths;
	// map from flow Id to path Id
	std::map<int, Session> flowIdToPathId;

        std::map<int, FlowInfo> flowStats;

        std::map<int, FlowInfo> activeFlows;
        std::vector<CharnyMakeFlowMsg*> flowMsgs;
        std::vector<CharnyMakeFlowMsg*> endFlowMsgs;

	cTopology* topo;
	std::map<int, RoutingTable> rtables;
	std::map<int, NextHopTable> nhtablesByPort;
	std::map<int, NextHopTable> nhtablesByAddr;

    protected:
      double getLongestRtt() {return longestRtt;}
      std::map<int, FlowInfo> allFlows;

      virtual void initialize();
      void initializeLinkCapacitiesAndDelays();
      void initializeLongestRtt();
      // TODO(lav): Gotta make sure hash on flow
      //  id gives same path always.
      void initializeRoutingAndNextHopTables();

      void finish();
      virtual void handleMessage(cMessage *msg);
      void handleControl(CharnyMakeFlowMsg* flowMsg);

      

      void checkParameters(std::vector<std::string> defaultParams);
      void scheduleNextFlowMsgs();
      void initializeCharnySim1();  // in sim01-intro.cc (hotnets intro example, 1 link and two flows)

      void  initializeCharnySim15();  // in sim15-fct-onelink.cc (FCT EXPERIMENTS FOR HOTNETS)
      void  initializeCharnySim16();  // in sim16-fct-onelink-dctcp.cc (FCT EXPERIMENTS TO RUN IDEAL FOR NS2 DCTCP FLOWS FOR HOTNETS)
      void  initializeCharnySim17();  // in sim17-fct-tor.cc (FCT experiments for the tor topology)

      void sendFlowMsgs(std::vector<FlowInfo> flows);

      void emitSignal(std::string name, double value);
      void emitSignal(std::string name, const char* value);

      std::vector<std::string> statNames;
      void setupSignals();

      int nChoosek( int n, int k );

      CharnyMakeFlowMsg* getCharnyMakeFlowMsg(const char *msgName, double sendTime, bool start, int flowId,\
              int src, int dest);
      int checkOptimalsPerRTT;

 public:
 CharnyFlowGenerator(): topo(NULL), longestRtt(-1) {};

};


void orderFlowsByStartTime(std::vector<FlowInfo>& flows);
void orderFlows(std::vector<FlowInfo>& flows);

class SimpleFlowGenerator : public cSimpleModule
{
    private:
        GenericPacket* getGenericPacket();
    protected:
        virtual void initialize();
        void finish();
        virtual void handleMessage(cMessage *msg);
};

}; // namespace

#endif
