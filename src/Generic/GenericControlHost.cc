#include "GenericControlHost.h"
#include "GenericSwitch.h" // for updateHop
#include <assert.h>

namespace rcp {
Define_Module(GenericControlHost);

  // Notify Flow Generate when data rate changes
#define HANDLE_CHANGE_RATE 0
#define EMIT_SIGNALS 0

#ifndef EV1
#define EV1 if (false) EV

void GenericControlHost::initialize()
{
  if (EMIT_SIGNALS) {
    sourceReceivePacketSignal = registerSignal("sourceReceivePacket");
    startSignal = registerSignal("start");
    endSignal = registerSignal("end");
    lengthSignal = registerSignal("length");
  }

    address = getParentModule()->getIndex();
    header_size_ = par("headerSize").doubleValue();
    payload_size_ = par("payloadSize").doubleValue();
    pktSize = header_size_ + payload_size_;

    const char *vinit[] = {"StampedRate"};
    statNames =  std::vector<std::string>(vinit, vinit + sizeof(vinit)/sizeof(char *));

}

// TRY WITHOUT TIMEOUTS
void GenericControlHost::handleMessage(cMessage *msg)
{
    if (msg->arrivedOn("flowAlert$i")) { // from Data Host to start or end a flow
        CharnyMakeFlowMsg *flowmsg = check_and_cast<CharnyMakeFlowMsg *>(msg);
        EV1 << "Local " << address << ": Arrived on Flow Alert for flow # " << flowmsg->getFlowId() << "\n";
        assert( flowmsg->getChangeType() == CHARNY_MAKEFLOW_CHANGE_DEMAND or\
                flowmsg->getChangeType() == CHARNY_MAKEFLOW_START or\
                flowmsg->getChangeType() == CHARNY_MAKEFLOW_END);
        if (flowmsg->getChangeType() == CHARNY_MAKEFLOW_START) setupSession(flowmsg);
        else if (flowmsg->getChangeType() == CHARNY_MAKEFLOW_END) tearDownSession(flowmsg);
        else if (flowmsg->getChangeType() == CHARNY_MAKEFLOW_CHANGE_DEMAND) updateDemand(flowmsg);
    } else { // from other (control) host
        GenericControlPacket *oldpkt = check_and_cast<GenericControlPacket *>(msg);
        // HELLO or DATA or FIN packet
        if (oldpkt->getPktType() == CHARNY_HELLO || oldpkt->getPktType() == CHARNY_FIN) {
          if (!oldpkt->getIsForward()) {
	    GenericLinkState::updateHop(oldpkt);
	    protocolSpecificProcessIngress(oldpkt);
	  }
          recvHello(oldpkt);
        }
    }
    delete msg;

}

void GenericControlHost::tearDownSession(const CharnyMakeFlowMsg *msg) {
    int flowId = msg->getFlowId();
    if (pIActiveSessions.count(flowId) == 0) {
        EV1 << "Session # " << flowId << " doesn't have any state.\n";
        return;
    }
    // send FIN next time
    pIActiveSessions[flowId].nextPacket = CHARNY_FIN;
}

void GenericControlHost::updateDemand(const CharnyMakeFlowMsg *msg) {
    int flowId = msg->getFlowId();
    if (pIActiveSessions.count(flowId) == 0) {
        EV1 << "Session # " << flowId << " doesn't have any state.\n";
        return;
    }
    // update number of packets left to sent
    pIActiveSessions[flowId].numPacketsLeft = msg->getDuration();
    double packetSizeBits = 8 * (pktSize);
    assert(pIActiveSessions[flowId].minRtt > 0);
    double demand = round((msg->getDuration() * packetSizeBits)/pIActiveSessions[flowId].minRtt);
    assert(demand < INFINITE_RATE);
    EV1 << "Update demand of flow # " << flowId << " to "\
                << demand << " Bps "\
                << " since " << msg->getDuration() << " packets left."\
                << " (last demand was " << pIActiveSessions[flowId].demand << ").\n";
    pIActiveSessions[flowId].demand = demand;
}

void GenericControlHost::setupSession(const CharnyMakeFlowMsg *msg) {
    int flowId = msg->getFlowId();


    GenericControlHostSessionState session(INFINITE_RATE, true, flowId);

    session.destination = msg->getDestination();
    session.src = msg->getSource();
    session.flowmsg = *msg;
    session.minRtt = msg->getMinRtt();

    if (EMIT_SIGNALS) {
      setupSignals(session, flowId);
      EV1 << "setting up " << session.signals.size() << " signals for flow # "  << flowId << ".\n";
    }

    pIActiveSessions[flowId] = session;
    protocolSpecificSetupSession(msg);

    if (msg->getDuration() != -1) updateDemand(msg);

    EV1 << "Setup session for flow # " << flowId << " with demand "\
            << pIActiveSessions.at(flowId).demand << ".\n";
    // also need to setup session in destination, when it gets first Hello


    EV1 << "Session # " << flowId << " now has  state.\n";
    sendHello(flowId, pIActiveSessions.at(flowId).nextPacket);
}

void GenericControlHost::setupSignals(GenericControlHostSessionState& temp, int flowId) {
    for (const auto& statName : statNames ) {
        std::stringstream ss;
        ss.clear(); ss.str("");
        ss << "session-" << flowId << "-" << statName;
        temp.signals[ss.str()] = registerSignal(ss.str().c_str());

        std::stringstream ts;
        ts.clear(); ts.str("");
        ts << "session" << statName;

        cProperty *statisticTemplate =\
        getProperties()->get("statisticTemplate", ts.str().c_str());
        ev.addResultRecorders(this, temp.signals[ss.str()],\
                ss.str().c_str(), statisticTemplate);
    }

}

void GenericControlHost::emitSignal(int flowId, std::string name, double value) {
    std::stringstream ss; ss.clear(); ss.str("");
    ss << "session-" << flowId << "-" << name;
    emit(pIActiveSessions[flowId].signals[ss.str()], value);
    EV1 << "At time "<< simTime() << ", emitting " << ss.str() << ": " << value << "\n";
 }


void GenericControlHost::sendHello(int flowId, int pktType) {
    // source generates packet with session.stamped rate, session.markedBit
    // destination creates feedback packet with its ...
  
    GenericControlHostSessionState &session = pIActiveSessions[flowId];
    char pktname[200];
    int packetDestination = session.isForward ? session.destination :\
                                    session.src;
    std::string direction = session.isForward ? "fwd": "fb";
    sprintf(pktname, "control-%s-flow#-%d-from-%d-to-%d-%s",\
            charnyPktType[pktType].c_str(),\
            flowId,\
            address,\
            packetDestination,\
            direction.c_str());
            /* //-rate-%.2f-%smarked",\
            session.stampedRate,\
            session.isForward ? "": "un");
             */
    GenericControlPacket *pkt = 
      newProtocolSpecificControlPacket(
				       pktname, flowId);
    pkt->setFlowId(flowId);
    pkt->setSource(address);
    pkt->setDestination(packetDestination);

    pkt->setPktType(pktType);
    pkt->setByteLength(header_size_); // syn length

    pkt->setIsForward(session.isForward);
    pkt->setIsExit((pktType == CHARNY_FIN));

    // Forward packet- reset hop and maxHops
    if (session.isForward) {
      pkt->setHop(0);
      pkt->setMaxHops(0);
    } else {
    // Reverse packet- set hop and maxHops to maxHopsFromRxPkt
      assert(session.maxHopsFromRxPkt > 0);
      pkt->setHop(session.maxHopsFromRxPkt);
      pkt->setMaxHops(session.maxHopsFromRxPkt);
    }
    // either way use maxHopsFromRxPkt (if initialized) to initialized hops
    int maxHopsToInitialize = MAX_HOPS;
    if (session.maxHopsFromRxPkt > 0)
      maxHopsToInitialize = session.maxHopsFromRxPkt;

    // reset hops
    // pkt->setHop(0);
    for (int h = 0; h < maxHopsToInitialize; h++) {
    if (session.isForward)      
      pkt->setIngressPortOnForward(h, -1);
    else
      pkt->setIngressPortOnForward(h, session.ingressPortOnForward[h]);
    }
    //pkt->setMaxHops(0);
    
    //if (session.isForward)
    //  pkt->setLastHopOnForward(0);
    //else
    //  pkt->setLastHopOnForward(session.lastHopOnForward);
    
    updateProtocolSpecificControlPacket(pkt);

    EV1 << "Host " << pkt->getSource() << " sending " << charnyPktType[pkt->getPktType()]\
            << " for flow # " << flowId << " to "\
                << pkt->getDestination() << ".\n";
    session.numSent++;

    forwardPacket(pkt);
}


void GenericControlHost::recvHello(const GenericControlPacket *pkt) {
    int flowId = pkt->getFlowId();

    if (pIActiveSessions.count(flowId) == 0) {
        GenericControlHostSessionState session(INFINITE_RATE, false, flowId);
        session.src = pkt->getSource();
        session.destination = pkt->getDestination();
	session.maxHopsFromRxPkt = pkt->getHop();
        pIActiveSessions[flowId] = session;
        addProtocolSpecificActiveSession(pkt);
    }

    GenericControlHostSessionState &session = pIActiveSessions.at(flowId);
    if (session.isForward) {
      session.maxHopsFromRxPkt = pkt->getMaxHops();
    } else {
      session.maxHopsFromRxPkt = pkt->getHop();
    }

    // If this is the destination, update state at destination and send feedback
    // If this is the source, update state at source
    EV1 << "Host " << pkt->getDestination() << " received " 
       << charnyPktType[pkt->getPktType()]		\
       << " for flow # " << flowId << " from "		\
       << pkt->getSource() << ".\n";

    if (session.isForward) {
       EV1 << "Host " << pkt->getDestination() << " is the source for this flow.\n";
       std::stringstream ss;
       for (int i = 0; i < session.maxHopsFromRxPkt; i++)
	 ss << pkt->getIngressPortOnForward(i) << " ";

       EV1 << "EV: Packet came in at source of flow " 
	  << pkt->getFlowId() << " with "
	  << "hop: " << pkt->getHop()
	  << ", ingressPortOnForward: " << ss.str()
	  << ", maxHops: " << pkt->getMaxHops() << "\n";

       // TODO(lav): pass general session state to protocol specific too
       sourceReceivePacketGeneral(pkt);
       // Source keeping pinging back until it receives a FIN (ACK)
       if (pkt->getPktType() != CHARNY_FIN)
	 sendHello(flowId, session.nextPacket);
    } else if (!session.isForward) {
       EV1 << "Host " << pkt->getDestination() << " is the destination for this flow.\n";

       std::stringstream ss;
       for (int i = 0; i < session.maxHopsFromRxPkt; i++)
	 ss << pkt->getIngressPortOnForward(i) << " ";

       EV1 << "EV: Packet came in at destination of flow "
	  << pkt->getFlowId() << " with "
	  << "hop: " << pkt->getHop()
	  << ", ingressPortOnForward: " << ss.str()
	  << ", maxHops: " << pkt->getMaxHops() << "\n";

       for (int i = 0; i < session.maxHopsFromRxPkt; i++)
	 session.ingressPortOnForward[i] = 
	   pkt->getIngressPortOnForward(i);

       if (destinationReceivePacket(pkt)) {
         sendHello(flowId, pkt->getPktType());
       }
     }
     if (pkt->getPktType() == CHARNY_FIN) {
          pIActiveSessions.erase(pkt->getFlowId());
          removeProtocolSpecificActiveSession(pkt->getFlowId());
     }

}

  // TODO(lav): change name to rate change message
void GenericControlHost::sendFlowMsg(int flowId, double newRate) {
  GenericControlHostSessionState &session = pIActiveSessions.at(flowId);
    CharnyMakeFlowMsg* msg = new CharnyMakeFlowMsg(session.flowmsg);
    msg->setRate(newRate);
    msg->setChangeType(CHARNY_MAKEFLOW_CHANGE_RATE);
    EV1 << "After " << session.numRecvd << " packets, sending "\
    << (session.dataRate > newRate ? "decrease" : "increase")\
    << " rate message for flow # " << flowId\
    << " from " << session.dataRate << " Bps to "\
    << newRate << " Bps at time " << simTime().dbl() << ".\n" ;
    session.dataRate = newRate;
    // tell data host to update actual rate
    send(msg, "flowAlert$o");
}

void GenericControlHost::forwardPacket(GenericControlPacket *pkt)
{
    EV1 << "CharnyHost " << address << ": Forwarding message " << pkt << " on out\n";
    pkt->setKind(CHARNY_CONTROL_PKT);
    if (pkt->getIsForward())
      egressAction(pkt);
    send(pkt, "gate$o");
}

void GenericControlHost::updateTransmissionRate(const GenericControlPacket *msg) {
    // why is this so complicated??

    double incomingRate = getIncomingRate(msg);
    // if packet doesn't carry incoming rate ask host using flow id
    if (incomingRate == -1) incomingRate = getIncomingRateFromId(msg->getFlowId());

    GenericControlHostSessionState& session = pIActiveSessions.at(msg->getFlowId());

    EV1 << "EVH: Update transmission rate for flow  " << session.flowId\
            << ", incoming rate is  " << incomingRate\
            << ", existing rate is "  << session.dataRate << ".\n";

    if (par("conservative").doubleValue() == 0 or session.dataRate == 0) {
        EV1 << "Being aggressive.\n";
        if (! rateEqual(incomingRate, session.dataRate)) {
                EV1 << "incoming rate "<< incomingRate\
                        <<" is "\
                        << (rateGreaterThan(incomingRate,\
                                session.dataRate) ? "more" : "less")\
                        << " than actual rate " << session.dataRate << ", ";
                EV1 << "schedule rate change now  "  << session.numRecvd \
                        << " at time " << simTime().dbl() << ".\n";
                session.sendAtPktNum = session.numRecvd;
                session.sendDataRate = incomingRate;
            } else {
                EV1 << "Incoming stamped rate (min) is " << incomingRate\
                        << " but it is same as session data rate "\
                        << session.dataRate << ".\n";
            }
    } else if (par("conservative").doubleValue() > 0) {
        double numRtts = par("conservative").doubleValue();
        EV1 << "Being conservative.";
        if (! rateEqual(incomingRate, session.dataRate)) {
            // if incoming rate is different from actual rate
          if (rateGreaterThan(incomingRate, session.dataRate)) {
              // if incoming rate is more than actual rate
              EV1 << "incoming rate is more than actual rate " << session.dataRate << ", ";
              if (session.sendAtPktNum >= session.numRecvd) {
                  // if rate change is scheduled
                  if (rateGreaterThan(incomingRate, session.sendDataRate)) {
                      // if scheduled rate is smaller than incoming rate, send after two RTTs
                      EV1 << "incoming rate "<< incomingRate\
                              << "is more than existing rate increase "\
                              << session.sendDataRate << " scheduled (at RTT "\
                              << session.sendAtPktNum << " vs now " << session.numRecvd << ")"\
                              << " reschedule to " << numRtts << " RTTs from now at "\
                              << session.numRecvd+numRtts << ".\n";
                      session.sendAtPktNum = session.numRecvd+numRtts;
                      session.sendDataRate = incomingRate;
                    } else {
                        // if scheduled rate is smaller than incoming rate, stick to old schedule
                        EV1 << "incoming rate "<< incomingRate\
                        << " is not more than existing rate increase "\
                        << session.sendDataRate << " scheduled (at RTT "\
                        << session.sendAtPktNum << " vs now " << session.numRecvd << ")"\
                        << " stick to schedule.\n";
                      session.sendDataRate = incomingRate;
                    }
                } else {
                    // if no rate change is scheduled
                    EV1 << "No rate increase scheduled yet, schedule new rate increase to incoming rate "\
                            << incomingRate << " " << numRtts << " RTTs from now at "\
                            << session.numRecvd+numRtts << ".\n";
                    session.sendAtPktNum = session.numRecvd+numRtts;
                    session.sendDataRate = incomingRate;
                }
          } else {
              // if incoming rate is less than actual rate
              EV1 << "incoming rate is less than actual rate " << session.dataRate\
                      << ", schedule rate decrease now  "  << session.numRecvd << ".\n";
              session.sendAtPktNum = session.numRecvd;
              session.sendDataRate = incomingRate;
          }
        } else if (rateEqual(incomingRate, session.dataRate)) {
            EV1 << "rate is same as actual rate " << session.dataRate << ".\n";
        }
    }


    if (session.sendAtPktNum == session.numRecvd) {
        assert(session.sendDataRate >= 0);        
	sendFlowMsg(session.flowId, session.sendDataRate);
        //session.sendAtPktNum = -1;
    }

}



void GenericControlHost::egressAction(GenericControlPacket *msg) {
    int flowId = msg->getFlowId();

    double oldCapacity = getLinkCapacity(flowId);
    double newCapacity = pIActiveSessions.at(flowId).demand;
    // starts with infinite rate
    if (oldCapacity != newCapacity and newCapacity < INFINITE_RATE) {
        EV1 << "EVENT! Updated host's demand/link capacity for flow # " << msg->getFlowId() << " from "\
                << oldCapacity << " to " << newCapacity << ".\n";
        setLinkCapacity(flowId, pIActiveSessions.at(flowId).demand);	
    }

    assert(msg->getIsForward());
    protocolSpecificProcessEgress(msg);
    GenericLinkState::updateHop(msg);
}
#endif // EV1

}; // namespace
