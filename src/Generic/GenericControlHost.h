//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __GENERIC_CONTROL_HOST_H
#define __GENERIC_CONTROL_HOST_H

#include <omnetpp.h>
#include "util.h"
#include <map>
#include <utility>

namespace rcp {

enum CHARNY_HOST_SEND_REASON {DECREASE_FROM_INFINITY, INCREASE, DECREASE, NONE};
const std::string charnyHostSendReason[] = {"DECREASE_FROM_INFINITY", "INCREASE", "DECREASE", "NONE"};

// Protocol-specific Per-Session State at End Hosts
/*
class CustomControlHostSessionState {
protected:

public:
    int maxHops;
    CustomControlHostSessionState() : maxHops(0) {};
};
*/

// Generic Per-Session State at End Host for all Proactive Protocols
class GenericControlHostSessionState {
public:
    int flowId;
    int src;
    int destination;


    double demand;

    bool isForward; // Both src and dest. store this state
    double lastPktTime; // Valid for src, when it sent last hello

    int ingressPortOnForward[MAX_HOPS];
 
    // used by host when sending packet 
    // set after receive, reset after send
    int maxHopsFromRxPkt;
    //int lastHopOnForward; 

    int numSent;
    int numRecvd;
    enum CHARNY_PKT_T nextPacket;
    double dataRate;
    int sendAtPktNum;
    double sendDataRate;

    double minRtt;
    int numPacketsLeft;

    std::map<std::string, simsignal_t> signals;

    CharnyMakeFlowMsg flowmsg;
    GenericControlHostSessionState(double dem, bool isFwd, int fid) :
        flowId(fid), demand(dem),\
        isForward(isFwd), lastPktTime(-1),\
        numSent(0), numRecvd(0), nextPacket(CHARNY_HELLO),\
	  sendAtPktNum(-1), dataRate(0), sendDataRate(-1), minRtt(-1), numPacketsLeft(-1),
	  maxHopsFromRxPkt(0) {
	  for (int i = 0; i < MAX_HOPS; i++) 
	    ingressPortOnForward[i] = -1;
    }
    GenericControlHostSessionState() :
        flowId(-1), src(-1), destination(-1),\
        demand(INFINITE_RATE),isForward(true),\
        lastPktTime(-1), numSent(0), numRecvd(0), nextPacket(CHARNY_HELLO),\
	  sendAtPktNum(-1), dataRate(0), sendDataRate(-1), minRtt(-1), numPacketsLeft(-1),
	   maxHopsFromRxPkt(0) {
	  for (int i = 0; i < MAX_HOPS; i++) 
	    ingressPortOnForward[i] = -1;

    }
};



class GenericControlHost : public cSimpleModule
{

protected:
    simsignal_t sourceReceivePacketSignal;
    simsignal_t startSignal;
    simsignal_t endSignal;
    simsignal_t lengthSignal;
    int address; // CharnyHost index for now
    double header_size_;
    double payload_size_;
    int pktSize;

    // Charny state
    typedef std::map<int, GenericControlHostSessionState> SessionTable;
    SessionTable pIActiveSessions;
    std::vector<std::string> statNames;

    virtual void initialize();
    virtual void handleMessage(cMessage *msg);

    void setupSession(const CharnyMakeFlowMsg *msg); // sets up state, sendHello(HELLO)
    void emitSignal(int flowId, std::string name, double value);
    void setupSignals(GenericControlHostSessionState& temp, int flowId);

    void tearDownSession(const CharnyMakeFlowMsg *msg); // sendHello(FIN)
    void sendHello(int flowId, int pktType); // new CharnyPacket -> Host (HELLO/ FIN)

    void updateDemand(const CharnyMakeFlowMsg *msg);
    void recvHello(const GenericControlPacket *pkt); // updates rate, destination sendHello(HELLO)
    // also clear state if FIN

    // overloaded by some proactive schemes (Charny)

    void updateTransmissionRate(const GenericControlPacket *pkt);

    /* Updates session.linkState.linkCapacity based on flow's current demand
         * then calls linkAction on packet.
         */
    void egressAction(GenericControlPacket *msg);

    void forwardPacket(GenericControlPacket *pkt);
    void sendFlowMsg(int flowId, double newRate); // new CharnyMakeFlowMsg -> Data host

    // The following functions must be implemented by each specific pro-active protocol.

    virtual GenericControlPacket *newProtocolSpecificControlPacket(const char* pktname, int flowId){ \
        return new GenericControlPacket(pktname);
    }

    virtual void updateProtocolSpecificControlPacket
      (GenericControlPacket *pkt) {
      return;
    }

    virtual void sourceReceivePacket(const GenericControlPacket *pkt) {}; // assumes demand is always INFINITE
    virtual void sourceReceivePacketGeneral(const GenericControlPacket *pkt) {}; // demand could be FINITE, changing
    virtual bool destinationReceivePacket(const GenericControlPacket *pkt) {return true;};

    // for updateTransmissionRate
    virtual double getIncomingRate(const GenericControlPacket *msg) {return -1;}
    virtual double getIncomingRateFromId(int flowId) {return -1;}
    // for egressAction
    virtual void setLinkCapacity(int flowId, double cap) {}
    virtual double getLinkCapacity(int flowId) {return -1;}
    virtual void protocolSpecificProcessEgress(GenericControlPacket *msg) {};
    virtual void protocolSpecificProcessIngress(GenericControlPacket *msg) {};

    virtual void protocolSpecificSetupSession(const CharnyMakeFlowMsg *msg) {}; // at source
    virtual void addProtocolSpecificActiveSession(const GenericControlPacket *pkt) {}; // at destination
    virtual void removeProtocolSpecificActiveSession(int flowId) {}; // on FIN


};


}; // namespace

#endif
