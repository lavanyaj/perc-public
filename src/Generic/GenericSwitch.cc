#include "GenericSwitch.h"
#include "SwitchRoutingHelper.h"

#include <assert.h>

namespace rcp {

#ifndef EV2
#define EV2 EV
#ifndef EV1
#define EV1 EV //if (false) std::cout

Define_Module(GenericSwitch);
Define_Module(DummyFlowGenerator);

GenericLinkState::GenericLinkState():
        linkCapacity(-1), controlMsgsInQueue(0), dataMsgsInQueue(0),\
                dequeueMessage(NULL) {

            WATCH(linkCapacity);
            WATCH(controlMsgsInQueue);
            WATCH(dataMsgsInQueue);
        }

GenericLinkState::~GenericLinkState() {
}

double CustomLinkState::getAvailableLinkCapacity() {
    double linkCapacityForControl = (((header_size_*8)/minRtt) * (maxSessionsPerRtt));
    double linkCapacityForData = linkCapacity - linkCapacityForControl;
    if (linkCapacityForControl < 1) {
        linkCapacityForControl = 0.0;
        linkCapacityForData = linkCapacity;
    }
   return linkCapacityForData;
}

// TODO(lav): How to update hops
// packet has hop and maxHops field
// Initially, source sets hop and maxHops to 0 before sending a new packet
// destination leaves hop unchanged (aka sMSession.maxHops)
//   and sets maxHop to sMSession.maxHops before sending a new packet
// and resets sMSession.maxHops to 0
// for a forward packet, link uses hop to index then at end increments hop only
// when forward packet gets to destination, destination sets sMSession.maxHops to hop
//  and uses this ..
// for a reverse packets, link decrements hop only, uses hop to index
// when reverse packets gets to source, source sets sMSession.maxHops to maxHops
//  and uses this..
// Thereafter source sets hop and maxHops to 0 before seding a new packet
// and resets sMSession.maxHops
void GenericLinkState::updateHop(GenericControlPacket* pkt) {

  //TODO(lav): make sure updating hop on reverse path doesn't break perc etc.
  //if(pkt->getIsForward()) 
  //if (pkt->getIsForward()) pkt->setLastHopOnForward(pkt->getHop());
  
  if (pkt->getIsForward()) {
    assert(pkt->getHop() < MAX_HOPS-1);
    pkt->setHop(pkt->getHop()+1);
  } else {
    assert(pkt->getHop() > 0);
    pkt->setHop(pkt->getHop()-1);
  }

  //if (pkt->getHop() > pkt->getMaxHops()) pkt->setMaxHops(pkt->getHop());

  EV << "EV GenericLinkState::updateHop "
     << " for flow " << pkt->getFlowId()
     << (pkt->getIsExit() ?  " EXIT " : "")
     << " set"
     << (pkt->getIsForward() ? " forward" : " reverse")
     << " packet hop to " << pkt->getHop()
     << " max hops was " << pkt->getMaxHops() << ".\n";
}

void GenericLinkState::makeId() {
    std::stringstream id;

    id.clear();
    id.str("");
    id << "switch-" << switchId\
                    << "-outputGate-" << gateIndex\
                    << "-ToSwitch-" << otherSwitchId;

    idStr = id.str();

}

std::string GenericLinkState::getId() {
  return idStr;
}

GenericSwitch::GenericSwitch(): idStr("Generic Switch xx") {};

GenericSwitch::~GenericSwitch() {

}

int compareMessagePriority(cObject *obj1, cObject *obj2) {
    cMessage *msg1 = (cMessage *) obj1;
    cMessage *msg2 = (cMessage *) obj2;
    std::pair<int, simtime_t> pair1(msg1->getKind(), msg1->getTimestamp());
    std::pair<int, simtime_t> pair2(msg2->getKind(), msg2->getTimestamp());
    if (pair1 < pair2) return -1;
    else if (pair1 == pair2) return 0;
    else return 1;
}

cMessage* GenericSwitch::protocolSpecificProcessIngress(int outGateIndex, cMessage* msg){
  EV << "In GenericSwitch::protocolSpecificProcessIngress(" << outGateIndex << ", ..)\n";
  return msg;}
cMessage* GenericSwitch::protocolSpecificProcessEgress(int outGateIndex, cMessage* msg){
    EV << "In GenericSwitch::protocolSpecificProcessEgress(" << outGateIndex << ", ..)\n";
    return msg;}

void GenericSwitch::finish() {
    EV << "Clearing Link State for Forwarding/ Switching (PI).\n";
    for (auto ls : ltable) {
        cMessage* dequeueMessage = ls.second->dequeueMessage;
        if (dequeueMessage) {
            if (dequeueMessage->isScheduled()) {
                EV << "Cancel dequeue packet permanently.\n";
                cancelAndDelete(dequeueMessage);
             } else {
                delete dequeueMessage;
             }
            ls.second->dequeueMessage = NULL;
        }

        delete ls.second;
        ls.second = NULL;
    }
    ltable.clear();
}

  std::queue<std::vector<int> >
  GenericSwitch::getShortestPathsToNode(int i, cTopology* topo) {
    cTopology::Node *thisNode = topo->getNodeFor(getParentModule());
    int thisTopoId = thisNode->getModule()->getIndex();
    std::string thisNodeType = thisNode->getModule()->getNedTypeName();
    topo->calculateUnweightedSingleShortestPathsTo(topo->getNode(i));

    EV << "EV GenericSwitch::getShortestPathsToNode(" << i << ", topo) for " << this->getId() << "\n";

    std::queue<std::vector<int> > paths;    
    if (thisNode->getNumPaths()==0) return paths;

    std::vector<int> start;
    start.push_back(thisTopoId);
    paths.push(start);

    int numPathsPopped = 0;

    while (paths.size() > 0) {

      auto& path = paths.front();

      auto last = path.back();
      auto lastNode = topo->getNode(last);
      assert(thisNodeType.compare(
				  lastNode->getModule()
				  ->getNedTypeName()) == 0);
	     
      auto lastDistance = lastNode->getDistanceToTarget();

      EV << "EV ShortestPaths " << this->getId() 
	 << ": In getShortestPathsToNode(" << i << ", " << thisTopoId << ")"
	 << " Step " << numPathsPopped 
	 << ", Now we have " << paths.size() << " in-progress paths "
	 << lastDistance << " steps away from " << i
	 << ". Current path has last node " << last 
	 << "\n.";

      // this path and paths that follow end at target
      if (lastDistance == 0)
	break;

      
      std::map<int, int> closerNeighbors;
      for (int l=0; l< lastNode->getNumOutLinks(); l++) {            
	cTopology::LinkOut* linkOut = lastNode->getLinkOut(l); assert(linkOut);
	cTopology::Node* neighborNode = linkOut->getRemoteNode(); assert(neighborNode);
	std::string neighborNodeType = neighborNode->getModule()->getNedTypeName();
	EV << "EV ShortestPaths " << this->getId()
	   << ": neighbor node " << neighborNode->getModule()->getIndex()
	   << " has type " << neighborNodeType << " and is " 
	   << neighborNode->getDistanceToTarget()
	   << " away from target.\n";
	if (neighborNodeType.compare(thisNodeType) != 0) 
	  continue;      
	
	auto neighborId = neighborNode->getModule()->getIndex();
	auto neighborDistance = neighborNode->getDistanceToTarget();
	if (neighborDistance == lastDistance-1)
	  closerNeighbors[neighborId] = neighborDistance;	
      }
      
      EV << "EV ShortestPaths " << this->getId() 
	 << ": Found " << closerNeighbors.size() << " neighbors"
	 << " of last node at distance " << lastDistance-1
	 << " from target.\n";

      for (const auto& neighbor : closerNeighbors) {
	std::vector<int> newPath(path);
	newPath.push_back(neighbor.first);
	std::stringstream pathSs;
	for (const auto& p : newPath)
	  pathSs << p << " ";

	EV << "EV ShortestPaths " << this->getId() 
	   << "Added a new path " << pathSs.str() 
	   << " ending in node " << path.back()
	   << " which is at a distance " 
	   << topo->getNode(path.back())->getDistanceToTarget()
	   << " from target.\n ";
	paths.push(newPath);
      }

      paths.pop();
      numPathsPopped++;
    }
    return paths;   
  }

void GenericSwitch::initialize()
{
    address = getParentModule()->getIndex();
    numLinks = gateSize("port");

    std::stringstream idSs;
    idSs << "Generic Switch " << address;
    idStr = idSs.str();

    // From Routing.cc
    cTopology *topo = new cTopology("topo");

    std::vector<std::string> nedTypes;
    nedTypes.push_back(getParentModule()->getNedTypeName());

    topo->extractByNedTypeName(nedTypes);
    EV << "Node " << address << ": cTopology found " << topo->getNumNodes() << " nodes\n";

    cTopology::Node *thisNode = topo->getNodeFor(getParentModule());
    std::string thisNodeType = thisNode->getModule()->getNedTypeName();
    // getModule: cNode -> cModule
    // getParent: cModule -> cNode
    // this = cModule

    // get this node's ID
    int thisTopoId = -1;
    for (int i=0; i<topo->getNumNodes(); i++) {
      if (topo->getNode(i) == thisNode) {
	thisTopoId = i; 
	break;}
    }
    int src = thisNode->getModule()->getIndex();
    assert(address == src);
    assert(thisTopoId != -1);
    assert(thisTopoId == src);
    

    // find gates towards each neighboring node of thisTopoId
    std::stringstream gateTowardsSs;
    std::map<int, int> gateTowards; // by addrs
    std::map<int, int> gateTowardsByPort;
    getNextHopTable(topo, src, gateTowardsByPort, gateTowards);
    getRoutingTable(topo, src, gateTowards, rtable, idStr);

    EV << "routing table for node " << address << endl;
    for(RoutingTable::iterator it1 = rtable.begin(); it1 != rtable.end(); ++it1) {
        int host = it1->first;
        EV << host << ":";
        for(std::vector<int>::iterator it2 = (it1->second).begin(); it2 != (it1->second).end(); ++it2) {
            EV << " g" << *it2 << ",";
        }
        EV << endl;
        EV << "random gates to host " << host << ":";
        for(int j=0; j < 4; j++) {
            EV << " " << findOutGate(host);
        }
        EV << endl;
    }

    const char *vinit[] = {"QueueLength", "ControlMsgsInQueue", "DataMsgsInQueue"};
    statNames =  std::vector<std::string>(vinit, vinit + sizeof(vinit)/sizeof(char *));;

    int numLinks = thisNode->getNumOutLinks();
    assert(numLinks > 0);

    for (int l= 0; l < numLinks; l++) {
        cTopology::LinkOut* linkOut = thisNode->getLinkOut(l);
        assert(linkOut);
        cTopology::Node* otherNode = linkOut->getRemoteNode();
        assert(otherNode);
        std::string otherNodeType = 
	  otherNode->getModule()->getNedTypeName();
        if (otherNodeType.compare(thisNodeType) != 0) continue;
        int otherTopoId = otherNode->getModule()->getIndex();
        GenericLinkState *ls = new GenericLinkState();
        ls->switchId = address;
        ls->gateIndex = l;
        ls->otherSwitchId = otherTopoId;
        ls->makeId();
        setupSignals(*ls);
        ltable[l] = ls;
        EV1 << "Added new link state for link " 
	    << l << " from " << thisNodeType << " # " << address 
	    << " to " << otherNodeType << " # " << otherTopoId << ".\n";
    }

    EV1 << "EV: Link state table of " << this->getId() << " has " <<
      ltable.size() << " entries.\n";

    for (auto& ls: ltable) {
      int gateIndex = ls.first;
      auto& linkState = ls.second;
      cDatarateChannel *chan = check_and_cast<cDatarateChannel *>	\
	(getParentModule()->gate("port$o", gateIndex)->getChannel());
      double linkCapacity = chan->getDatarate();
      linkState->linkCapacity = linkCapacity;
      linkState->dropSignal = registerSignal("drop");
      linkState->outputIfSignal = registerSignal("outputIf");
      linkState->dequeueMessage = new CharnyDequeueMessage("dequeueMessage");
        linkState->dequeueMessage->setOutGateIndex(gateIndex);

        EV << "data rate of port # " << gateIndex << " is " << chan->getDatarate() << "Bps,"\
                << " delay is " << SIMTIME_DBL(chan->getDelay()) << "s.\n";
        EV << "setting link capacity of gate # " << gateIndex << " to " << linkCapacity << ".\n";

    }


    for (auto& ls: ltable) {
      int gateIndex = ls.first;
      auto& linkState = ls.second;
      CharnyDequeueMessage *dMsg = ltable[gateIndex]->dequeueMessage;
      EV2 << "outGateIndex # " << gateIndex				\
	  << " corresponds to link (" << ltable[gateIndex]->switchId	\
	  <<  ", "<< ltable[gateIndex]->otherSwitchId << "). "		\
	  << " Message has gateIndex " <<				\
	dMsg->getOutGateIndex() << ".\n";
      
      outputQueues.push_back(cQueue("", compareMessagePriority));
    }


    //nextOutGateIndex = 0;
    delete topo;
}

void GenericSwitch::setupSignals(GenericLinkState& temp) {
    for (const auto& statName : statNames ) {
        std::stringstream ss;
        ss.clear(); ss.str("");
        ss << "switch-" << temp.switchId\
                << "-outputGateTo-" << temp.otherSwitchId
                << "-" << statName;
        temp.signals[ss.str()] = registerSignal(ss.str().c_str());

        std::stringstream ts;
        ts.clear(); ts.str("");
        ts << "switchOutputGateTo" << statName;

        cProperty *statisticTemplate =\
        getProperties()->get("statisticTemplate", ts.str().c_str());
        ev.addResultRecorders(this, temp.signals[ss.str()],\
                ss.str().c_str(), statisticTemplate);
    }

}

void GenericSwitch::emitSignal(int gateIndex, std::string name, double value) {
    std::stringstream ss; ss.clear(); ss.str("");
    ss << "switch-" << ltable[gateIndex]->switchId << "-outputGateTo-" << ltable[gateIndex]->otherSwitchId << "-" << name;
    emit(ltable[gateIndex]->signals[ss.str()], value);
    EV1 << "At time "<< simTime() << ", emiting " << ss.str() << ": " << value << "\n";
 }


  // TODO(lav): This piece of code is sphagetti, please fix.

// TODO dequeueMessage has output queue #, vector of dequeue messages
// on dequeueMessage .. call dequeue(output queue #)
// on dequeue(output queue #), check if queue is empty ..
// on fromPort or fromHost .. and !dequeue[output queue #] scheduled..
void GenericSwitch::handleMessage(cMessage *msg)
{
    EV1 << "GenericSwitch"<< this->getId() <<": handleMessage(" << msg->getName() << ").\n";


    // isSelfMessage not working..

    // HANDLE DEQUEUE MESSAGE,
    // MESSAGE DEQUEUED FROM EGRESS QUEUE OF SWITCH
    // IS ALWAYS MEANT FOR ANOTHER SWITCH,
    // JUST PROCESS EGRESS AND SEND IT. DONE
    if (strcmp(msg->getName(), "dequeueMessage") == 0) {
      EV1 << "Got a dequeue message for link (..)!! ";
      int outGateIndex = -1;
      CharnyDequeueMessage *dMsg = check_and_cast<CharnyDequeueMessage *>(msg);
      outGateIndex = dMsg->getOutGateIndex();
      EV1 << "Got a dequeue message for link (" << ltable[outGateIndex]->switchId \
	  <<  ", "<< ltable[outGateIndex]->otherSwitchId << ")!! ";

      cMessage *newmsg = dequeue(outGateIndex);

      if (newmsg) {
        GenericPacket *newpkt = check_and_cast<GenericPacket *>(newmsg);
        int destination = newpkt->getDestination();

	EV1 << "QueueOut " << ltable[outGateIndex]->getId() 
	    << " -> Switch (" << this->getId()
	    << "): Send from Switch to Switch " 
	    << destination << ".\n";

	// All hop related stuff in GenericSwitch/ End Host
	// like setting up ingress port for routing back on same path
	// updating hop field so protocol can index into right values etc.
	if (newpkt->getKind() == CHARNY_CONTROL_PKT) {
	  GenericControlPacket* newctrlpkt
	    = check_and_cast<GenericControlPacket *>(newpkt);

	  // Egress processing only for forward packets
	  if (newctrlpkt->getIsForward()) {
	    newmsg = protocolSpecificProcessEgress(outGateIndex, 
						   newctrlpkt);
	    GenericLinkState::updateHop(newctrlpkt); // okay ^ just returns ptr
	  }
	}
	// outGateIndex info from dequeue packet
	simtime_t finishTime = sendToOther(newmsg, outGateIndex);
	assert(finishTime >= simTime());
	finishTime = std::max(finishTime, simTime()); // why? finishTime always in the future unless not transmitting

	if (!ltable[outGateIndex]->dequeueMessage->isScheduled()) {
	  EV1 << "On sendToOther, scheduled dequeue "			\
	    "from link (" << ltable[outGateIndex]->switchId		\
	      <<  ", "<< ltable[outGateIndex]->otherSwitchId		\
	      << ") at " << SIMTIME_DBL(finishTime) << ".\n";
	  scheduleAt(finishTime, ltable[outGateIndex]->dequeueMessage);
	}

      } else { // otherwise dequeueMessage is no longer scheduled
	EV1 << "Output queue is empty.\n";
          }
      return;
    } // ends if (strcmp(msg->getName(), "dequeueMessage") == 0)
    

    EV1 << "Not a dequeue message.\n";
    // OTHER MESSAGES ARE MESSAGES FROM HOST
    // OR MESSAGE FROM ANOTHER SWITCH ON INGRESS
    // OR MESSAGE FROM FLOW GENERATOR

    GenericPacket *pkt = check_and_cast<GenericPacket *>(msg);
    if ( pkt->hasBitError() ) {
        EV << "Packet has Bit Error! Dropping packet\n";
        emit(dropSignal, (long)pkt->getByteLength());
        return;
    }

    // Get ingateIndex for any packet from a port
    int inGateIndex = -1;
    // TODO(lav): too many pkts and msgs in this function, cleanup
    int source = pkt->getSource();

    // getInInfo(msg, inGateIndex, source);
    // There is an inGateIndex if it came fromPort()
    if (fromPort(msg)) {
      cGate* arrivalGate = msg->getArrivalGate();
      assert(arrivalGate->isVector());
      inGateIndex = arrivalGate->getIndex();
      assert(inGateIndex >= 0);
      assert(inGateIndex < arrivalGate->getVectorSize());
      // TODO(lav): check gate, index is valid
    }

    // Get outGateIndex for any packet to other
    int destination = pkt->getDestination();

    // get set of output ports (from rtable if data/ forward control
    // and from packet if reverse control)
    std::vector<int> candidatePorts;

    // pkt from src/destination at switch on same node
    // outGateIndex = local$ something
    if (toOther(msg) 
	&& pkt->getKind() == CHARNY_CONTROL_PKT) {
      GenericControlPacket *ctrlpkt = 
	check_and_cast<GenericControlPacket *>(msg);

      if (!ctrlpkt->getIsForward()) {
	assert(ctrlpkt->getHop() >= 0);
	//assert(ctrlpkt->getLastHopOnForward() >= 0);

      // at switch after dest: hop 0, lastHopOnForward 2, arr: 0 0 x
      // first +1 at coming up below after processIngress
	// For packet from host, use getHop = lastHop
	// For packet after, use getHop+1 (cuz +1 only coming up)
	int hopIndex = ctrlpkt->getHop();
	//ctrlpkt->getLastHopOnForward() -  ctrlpkt->getHop();
	if (!fromHost(msg)) hopIndex--;

	//assert(hopIndex >= 0 && hopIndex <= ctrlpkt->getLastHopOnForward());
	int hop = ctrlpkt->getIngressPortOnForward(hopIndex);
	assert(hop != -1);
	
	// check that hop index is valid
	RoutingTable::iterator it = rtable.find(destination);
	assert(it != rtable.end());

	bool valid = false;
	for (const auto& port: it->second)
	  if (port == hop) valid = true;
	if (!valid) {
	  std::stringstream validSs;
	  for (const auto& port : it->second)
	    validSs << port << " ";

	  EV << "EV GenericSwitch " << this->getId() 
	     << " OutGate " << hop << " from IngressPort[" << hopIndex 
	     << "] not valid"
	     << " for reverse packet of flow " << pkt->getFlowId() 
	     << " to Other"
	     << ". Routing table entry for destination " << destination
	     << " has " << it->second.size() << " entries " << validSs.str()
	     << " but " << hop << " is not one of them.\n";

	}

	assert(valid);
	candidatePorts.push_back(hop);
	
	EV << "EV GenericSwitch " << this->getId() 
	   << " Got outGate " << hop << " from IngressPort[" << hopIndex
	   << "] for reverse packet of flow " << pkt->getFlowId() 
	   << " to Other.\n";
      }
    } 

    if (candidatePorts.size() == 0 && toOther(msg)) {     
	RoutingTable::iterator it = rtable.find(destination);
	assert(it != rtable.end());

	std::stringstream ss;
	for (auto& port: it->second) {
	  if (port != inGateIndex) {
	    ss << port << " ";
	    candidatePorts.push_back(port);
	  }}

	if (pkt->getKind() == CHARNY_CONTROL_PKT) {
	  GenericControlPacket* ctrlpkt = check_and_cast<GenericControlPacket *>(msg);
	  assert(ctrlpkt->getIsForward());
	  EV << "EV GenericSwitch: " << this->getId()
	     << " Got outGates " << ss.str() << " from routing table "
	     << " for forward packet of flow " << ctrlpkt->getFlowId()
	     << " to Other that came in at "<< inGateIndex <<  " .\n";
	}

	assert(candidatePorts.size() > 0);
    }

    
    // TODO(lav): get outGateIndex for control/ data packets
    int outGateIndex = -1;
    if (toOther(msg)) {
      assert(candidatePorts.size() > 0);
      int hashval = std::hash<int>()(pkt->getFlowId());
      outGateIndex = candidatePorts[(hashval % candidatePorts.size())];
    }
    //, destination;
    //getOutInfo(msg, outGateIndex, destination);


    if (fromPort(msg) and toHost(msg)) { // came from queue
      assert(outGateIndex == -1);
      assert(inGateIndex != -1);
      EV << "EV GenericSwitch " << this->getId() << " : Port [" << inGateIndex <<  "]-> Host"
	 << "): Send packet of flow " << pkt->getFlowId() << " from Switch to Host.\n";

        if (msg->getKind() == CHARNY_CONTROL_PKT) {
		GenericControlPacket* pkt = check_and_cast<GenericControlPacket *>(msg);
		std::stringstream forwardSs;

		if (pkt->getIsForward()) forwardSs << "forward"; else forwardSs << "reverse";

		EV << "EV GenericSwitch: " << this->getId()
		   << " Got outGate local$o (to Host)"
		   << " " << forwardSs.str() <<" packet of flow " << pkt->getFlowId()
		   << " to Other that came in at "<< inGateIndex <<  " .\n";

		// Set ingress port for forward packets (used by reverse packet)
		assert(inGateIndex != -1);
		assert(pkt->getHop() >= 0);
		if (pkt->getIsForward()) {
		  assert(pkt->getHop() > 1); 
		  // since +1 at src-switch egress on same node 
		  //  and +1 on src/ switch node - next switch node (could be dst)
		  pkt->setIngressPortOnForward(pkt->getHop()-1, inGateIndex);
		}

		// Ingress processing only for reverse packets
		if (!pkt->getIsForward()) {
		  GenericLinkState::updateHop(pkt);
		  msg = protocolSpecificProcessIngress(inGateIndex, msg);
		}
	}
        sendToHost(msg);
        return;
    } else if ((fromPort(msg) or fromHost(msg)) and toOther(msg)) {
      assert(outGateIndex != -1);
      if (fromPort(msg)) {
	assert(inGateIndex != -1);
        EV << "EV GenericSwitch " << this->getId() << " : Port [" << inGateIndex 
	   << "] -> Other Switch " 
	   << ": Send packet of flow " << pkt->getFlowId() << " from Switch "
	   << " to Egress Queue/ Link " << ltable[outGateIndex]->getId() << ".\n";

      } else {
	assert(fromHost(msg));
	assert(inGateIndex == -1);
        EV << "EV GenericSwitch "<< this->getId() << " : Host -> Other Switch"
	   << ": Send packet of flow " << pkt->getFlowId() << " from Switch "
                << " to Egress Queue/ Link " << ltable[outGateIndex]->getId() << ".\n";	
      }

      if (fromPort(msg) and msg->getKind() == CHARNY_CONTROL_PKT) {
	GenericControlPacket* pkt = check_and_cast<GenericControlPacket *>(msg);

	// Set ingress port for forward packets (used by reverse packet)
        assert(inGateIndex != -1);
	assert(pkt->getHop() >= 0);
	if (pkt->getIsForward()) {
	  assert(pkt->getHop() > 1); 
	  // since +1 at src-switch egress on same node 
	  //  and +1 on src/ switch node - next switch node (could be dst)
	  pkt->setIngressPortOnForward(pkt->getHop()-1, inGateIndex);
	}
	// Message from host to switch on same node could be reverse too
	//  but no link there. Only links (for the purpose of rate allocation) are
	//   source to switch on same node (to set flow demand)
	//   source node to next switch..
	//   second to last switch to destination node

	// Ingress processing only for reverse packets
	if (!pkt->getIsForward())
	  msg = protocolSpecificProcessIngress(inGateIndex, msg);
      }
      // TODO(lav): get correct outGateIndex
      enqueue(msg, outGateIndex);
      if (!ltable[outGateIndex]->dequeueMessage->isScheduled()) {
	EV1 << "On enqueue, scheduled dequeue "				\
	    << "from link (" << ltable[outGateIndex]->switchId		\
	    <<  ", "<< ltable[outGateIndex]->otherSwitchId << ")"	\
	  " at " << SIMTIME_DBL(simTime()) << ".\n";
            scheduleAt(simTime(), ltable[outGateIndex]->dequeueMessage);
        } else {
	EV1 << "Dequeue from link (" << ltable[outGateIndex]->switchId	\
	    <<  ", "<< ltable[outGateIndex]->otherSwitchId << ") already scheduled.\n";
      }
    } else {
        cGate * gate = msg->getArrivalGate();
        EV << "Don't know what to do with packet arrived on "\
                << gate->getFullName() << " going to " << destination\
                << " through output queue # " << outGateIndex << ".\n";

    }

    //delete pkt;
    }


bool GenericSwitch::fromPort(cMessage* msg) {
    bool ret = msg->arrivedOn("port$i");
    if (ret) EV1 << "Packet from input queue/ port.\n";
    return ret;
}
bool GenericSwitch::fromHost(cMessage* msg) {
    bool ret = msg->arrivedOn("local$i") || msg->arrivedOn("dataLocal$i");
    if (ret) EV1 << "Packet from host.\n";
    return ret;
}
bool GenericSwitch::fromFlowGen(cMessage* msg) {
    bool ret = msg->arrivedOn("flowAlert");
    if (ret) EV1 << "Packet from flow generator.\n";
    return ret;
}


bool GenericSwitch::toOther(cMessage* msg) {
  
  if (strcmp(msg->getName(), "dequeueMessage") == 0) return false;
  // Should work for bota and control packets
  GenericPacket *pkt = check_and_cast<GenericPacket *>(msg);
  assert(pkt->getKind() == CHARNY_CONTROL_PKT 
	 or pkt->getKind() == CHARNY_DATA_PKT_SYN
	 or pkt->getKind() == CHARNY_DATA_PKT);
  int destination = pkt->getDestination();
  bool routable = rtable.find(destination) != rtable.end();
  bool ret = (destination != address and routable);
  /* int outGateIndex, destination;
    getOutInfo(msg, outGateIndex, destination);
    bool ret = (destination != address) and outGateIndex != -1;
  */ 
    if (ret) EV1 << "Packet to other.\n";
    return ret;
}

bool GenericSwitch::toHost(cMessage* msg) {
  if (strcmp(msg->getName(), "dequeueMessage") == 0) return false;
  // Should work for bota and control packets
  GenericPacket *pkt = check_and_cast<GenericPacket *>(msg);
  assert(pkt->getKind() == CHARNY_CONTROL_PKT 
	 or pkt->getKind() == CHARNY_DATA_PKT_SYN
	 or pkt->getKind() == CHARNY_DATA_PKT);
  int destination = pkt->getDestination();
  bool ret = (destination == address);
  /*
    int outGateIndex, destination;
    getOutInfo(msg, outGateIndex, destination);
    bool ret = (destination == address);
  */
    if (ret) EV1 << "Packet to host.\n";
    return ret;
}

simtime_t GenericSwitch::sendToOther(cMessage* msg, int outGateIndex) {
  /*
    int outGateIndex, destination;
    getOutInfo(msg, outGateIndex, destination);
  */
    cDatarateChannel *chan = check_and_cast<cDatarateChannel *> \
                            (getParentModule()->gate("port$o", outGateIndex)->getChannel());
    send(msg, "port$o", outGateIndex);
    return chan->getTransmissionFinishTime();
}

void GenericSwitch::sendToHost(cMessage* msg) {
    if (msg->getKind() == CHARNY_DATA_PKT or msg->getKind() == CHARNY_DATA_PKT_SYN)
        send(msg, "dataLocal$o");
    else send(msg, "local$o");
}

void GenericSwitch::enqueue(cMessage* msg, int outGateIndex) {
  /*int outGateIndex, destination;
    getOutInfo(msg, outGateIndex, destination);
  */
    msg->setTimestamp(simTime());
    EV1 << "Enqueued " << charnyPktTypeStr[msg->getKind()] << " message " <<\
            " onto output queue # " << outGateIndex <<\
            " with timestamp " << msg->getTimestamp() << ".\n";
    outputQueues[outGateIndex].insert(msg);
    emitSignal(outGateIndex, "QueueLength", outputQueues[outGateIndex].getLength());
            EV1 << "At time "<< simTime() << ", emitting outputGate"<< outGateIndex\
                    <<"-QueueLengthSignal: " << outputQueues[outGateIndex].getLength() << "\n";

    if (msg->getKind() == CHARNY_CONTROL_PKT or msg->getKind() == CHARNY_DATA_PKT_SYN) {
        ltable[outGateIndex]->controlMsgsInQueue++;
        emitSignal(outGateIndex, "ControlMsgsInQueue", ltable[outGateIndex]->controlMsgsInQueue);
    } else if (msg->getKind() == CHARNY_DATA_PKT) {
        ltable[outGateIndex]->dataMsgsInQueue++;
        emitSignal(outGateIndex, "DataMsgsInQueue", ltable[outGateIndex]->dataMsgsInQueue);
      }

    //send(msg, "ingress_end", outGateIndex); // store in queue
}

cMessage *GenericSwitch::dequeue(int nextOutGateIndex) {

    cMessage *ret;

    if (outputQueues[nextOutGateIndex].isEmpty()) {
        EV1 << "Output queue # "<< nextOutGateIndex << "  is empty.\n";
        ret = NULL;
    } else {

        ret = (cMessage *) outputQueues[nextOutGateIndex].pop();
        emitSignal(nextOutGateIndex, "QueueLength", outputQueues[nextOutGateIndex].getLength());
        EV1 << "At time "<< simTime() << ", emiting outputGate"<< nextOutGateIndex\
                <<"-QueueLengthSignal: " << outputQueues[nextOutGateIndex].getLength() << "\n";

        if (ret->getKind() == CHARNY_CONTROL_PKT or ret->getKind() == CHARNY_DATA_PKT_SYN) {
                ltable[nextOutGateIndex]->controlMsgsInQueue--;
                emitSignal(nextOutGateIndex, "ControlMsgsInQueue", ltable[nextOutGateIndex]->controlMsgsInQueue);
         } else if (ret->getKind() == CHARNY_DATA_PKT) {
                ltable[nextOutGateIndex]->dataMsgsInQueue--;
                emitSignal(nextOutGateIndex, "DataMsgsInQueue", ltable[nextOutGateIndex]->dataMsgsInQueue);
        }

        EV1 << "Dequeued " << charnyPktTypeStr[ret->getKind()] << " message " <<\
                " from output queue # " << nextOutGateIndex <<\
                " with timestamp " << ret->getTimestamp() << ".\n";
    }
    return ret;
}

int GenericSwitch::findOutGate(int destAddr)
{
    RoutingTable::iterator it = rtable.find(destAddr);
    if (it==rtable.end())
    {
        return -1;
    }
    std::vector<int> rpaths = it->second;
    return select_randomly(&rpaths); // outGateIndex
}

int GenericSwitch::findOutGate(int destAddr, GenericPacket *pkt)
{
    // hash on flowId only.
    // can extend to source/destination but do not use for now.
    int hashval = std::hash<int>()(pkt->getFlowId());
    RoutingTable::iterator it = rtable.find(destAddr);
    if (it==rtable.end())
    {
        return -1;
    }
    std::vector<int> rpaths = it->second;
    EV << "flow id " << pkt->getFlowId() << ", hashval " << hashval << ", mod " << (hashval % rpaths.size()) << endl;
    return rpaths[(hashval % rpaths.size())];
}

void DummyFlowGenerator::initialize() {

}
void DummyFlowGenerator::finish() {

}
void DummyFlowGenerator::handleMessage(cMessage *msg) {

}
#endif // EV1
#endif // EV2
}; // namespace


// CharnySim8 original simulation ended at 157427, t=0.02054944
