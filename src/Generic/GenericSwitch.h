//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __GENERIC_SWITCH_H
#define __GENERIC_SWITCH_H

#include <omnetpp.h>
#include "util.h"
#include "generic_m.h"
#include <map>
#include <queue>
#include <set>
namespace rcp {

class CustomSwitchSessionState {
    /*
     * CharnySwitchSessionState(); etc.
     */
};

class GenericLinkState {
    public:
    simsignal_t dropSignal;
    simsignal_t outputIfSignal;

    int switchId;
    int gateIndex;
    int otherSwitchId;
    std::string idStr;

    // Charny state
    double linkCapacity;
    bool eagerSwitchMode;

    int controlMsgsInQueue;
    int dataMsgsInQueue;
    CharnyDequeueMessage *dequeueMessage;

    std::map<std::string, simsignal_t> signals;

    GenericLinkState();
    ~GenericLinkState();
    std::string getId();
    void makeId();

    static void updateHop(GenericControlPacket* pkt);
};

class CustomLinkState {

protected:
    std::string idStr;
public:
    double linkCapacity;
        double header_size_;
        double payload_size_;
        double minRtt;
        double maxSessionsPerRtt;

    std::string getId() {return idStr;}
    CustomLinkState() :
        idStr(""), linkCapacity(-1), header_size_(-1), payload_size_(-1), minRtt(-1), maxSessionsPerRtt(0) {};
    CustomLinkState(std::string idStr):
        idStr(idStr), linkCapacity(-1), header_size_(-1), payload_size_(-1), minRtt(-1), maxSessionsPerRtt(0) {};
    double getAvailableLinkCapacity();


/*
 * private:
 * typedef std::map<int, CharnySwitchSessionState> SessionTable;
 *   SessionTable activeSessions
 *
 * public:
 *  CharnyLinkState(std::string idStr);
 *  ~CharnyLinkState();
 *  CharnyPacket *processIngress(CharnyPacket *pkt);
 *  CharnyPacket *processEgress(CharnyPacket *pkt);
 *
 */
};


class GenericSwitch : public cSimpleModule
{

    std::string idStr;
private:
    simsignal_t dropSignal;

public:
    RoutingTable rtable;
    int address; // CharnyHost index for now




    int numLinks;

    std::vector<cQueue> outputQueues;

    //int nextOutGateIndex;


    bool prioritizeControl;
    std::vector<std::string> statNames;


    typedef std::map<int,GenericLinkState*> GenericLinkStateTable; // output queue # -> link state
    GenericLinkStateTable ltable;
    // and a protocol specific link state table .., where for
    // each link, there is a protocol specific session state table

    std::string getId() {return idStr;}

    void initialize();
    void finish();
    void handleMessage(cMessage *msg);
    int findOutGate(int destAddr);
    int findOutGate(int destAddr, GenericPacket *pkt);


    bool fromPort(cMessage *msg);
    bool fromHost(cMessage *msg);
    bool fromFlowGen(cMessage *msg);
    bool toHost(cMessage *msg);
    bool toOther(cMessage *msg);
    simtime_t sendToOther(cMessage *msg, int outGateIndex);
    void sendToHost(cMessage *msg);

    void enqueue(cMessage *msg, int outGateIndex);
    cMessage *dequeue(int outGateIndex);
    //void scheduleDequeue();

    void setupSignals(GenericLinkState& temp);
    void emitSignal(int gateIndex, std::string name, double value);

    // The following functions must be implemented by specific protocols.

    virtual cMessage* protocolSpecificProcessIngress(int outGateIndex, cMessage* msg);
    virtual cMessage* protocolSpecificProcessEgress(int outGateIndex, cMessage* msg);

    
    std::queue<std::vector<int> >
      getShortestPathsToNode(int i, cTopology* topo);
    

public:
    GenericSwitch();
    ~GenericSwitch();
};

/*
 * Simply accepts ports for SwitchOnly..Doesn't do anything.
 */
class DummyFlowGenerator: public cSimpleModule
{
public:
    void initialize();
    void finish();
    void handleMessage(cMessage *msg);

};

}; // namespace

#endif
