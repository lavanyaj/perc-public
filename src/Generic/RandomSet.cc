#include "RandomSet.h"
#include <assert.h>
#include <iostream>



int testRandomSet() {
  RandomSet<int> flows;
  for (int i = 0; i < 10; i++) {
    flows.insert(2*i);
  }
  std::cout << "Random set now has " << flows.size() << " numbers.\n";

  for (int i = 0; i < 10; i++) {
    int elem = flows.getRandomElement();
    std::cout << elem << " ";
    flows.remove(elem);
  }
  std::cout << "\n";
  std::cout << "Random set now has " << flows.size() << " numbers.\n";
  return 0;
}

