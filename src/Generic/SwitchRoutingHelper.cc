#include "SwitchRoutingHelper.h"
#include <assert.h>

namespace rcp {

void getRoutingTable(cTopology* topo, int src,				\
		     const std::map<int, int>& nhtableByAddr,		\
		     std::map<int, std::vector<int> >& rtable,  
		     std::string& idStr) {
  rtable.clear();
  cTopology::Node* thisNode = topo->getNode(src);
  std::string thisNodeType 
    = thisNode->getModule()->getNedTypeName();
    
  for (int i=0; i<topo->getNumNodes(); i++) {
    if (i == src) continue;

    auto paths
      = getShortestPathsToNode(i, topo, src, idStr);

    if (paths.size() == 0) continue;

    int address = topo->getNode(i)->getModule()->getIndex();	    
    assert(address == i);

    std::stringstream gateSs;
    std::map<int, bool> seen;

    int numPaths = paths.size(); //thisNode->getNumPaths();
    int pathIndex = 0;
    while(paths.size() > 0) {
      auto path = paths.front();
      int neighborId = path.at(1);
      //cGate *parentModuleGate = thisNode->getPath(pathIndex)->getLocalGate();
      //int gateIndex = parentModuleGate->getIndex();
      assert(nhtableByAddr.count(neighborId) > 0);
      int gateIndex = nhtableByAddr.at(neighborId);
      if (seen.count(gateIndex) == 0) {
	seen[gateIndex] = true;
	gateSs << gateIndex << " ";
	rtable[i].push_back(gateIndex);
      }
      paths.pop();
    }

    EV << " " << numPaths << " shortest paths "
	<< " (length " << thisNode->getDistanceToTarget() << ")"
	<< " towards address " << i
	<< " from address " << src
	<< ", gateIndex vector is " << gateSs.str() << endl;
  }

  // print routing table
  std::stringstream rtableSs;
  for (const auto& entry: rtable) {
    rtableSs << entry.first << ": ";
    for (const auto& p: entry.second) {
      rtableSs << p << " ";
    }
    rtableSs << ", ";
  }
    
  EV << "EV GetRoutingTable: "
     << " Routing table at switch " << idStr
     << ": " << rtableSs.str() << "\n";
}


void getNextHopTable(cTopology* topo, int src,
		     std::map<int, int>& nhtableByPort,
		     std::map<int, int>& nhtableByAddr) {
  nhtableByPort.clear();
  nhtableByAddr.clear();

  cTopology::Node* thisNode = topo->getNode(src);
  std::string thisNodeType 
    = thisNode->getModule()->getNedTypeName();
  std::stringstream nhtableByPortSs;
  std::stringstream nhtableByAddrSs;

  for (int l=0; l< thisNode->getNumOutLinks(); l++) {            
    cTopology::LinkOut* linkOut = thisNode->getLinkOut(l); assert(linkOut);
    cTopology::Node* neighborNode = 
      linkOut->getRemoteNode(); assert(neighborNode);
    std::string neighborNodeType 
      = neighborNode->getModule()->getNedTypeName();
    if (neighborNodeType.compare(thisNodeType) != 0) continue;      
    int neighborId = neighborNode->getModule()->getIndex();

    assert(nhtableByAddr.count(neighborId) == 0);
    nhtableByAddr[neighborId] = l;
    nhtableByAddrSs << neighborId << ": " << l << " ";

    assert(nhtableByPort.count(l) == 0);
    nhtableByPort[l] = neighborId;
    nhtableByPortSs << l << ": " << neighborId;
  }

}

std::queue<std::vector<int> >\
getShortestPathsToNode(int i, cTopology* topo, int src,\
		       const std::string& idStr) {
  cTopology::Node *thisNode = topo->getNode(src);
  int thisTopoId = thisNode->getModule()->getIndex();
  std::string thisNodeType = thisNode->getModule()->getNedTypeName();
  topo->calculateUnweightedSingleShortestPathsTo(topo->getNode(i));

  EV << "EV GenericSwitch::getShortestPathsToNode(" << i 
     << ", topo) for " << idStr << "\n";

  std::queue<std::vector<int> > paths;    
  if (thisNode->getNumPaths()==0) return paths;

  std::vector<int> start;
  start.push_back(thisTopoId);
  paths.push(start);

  int numPathsPopped = 0;

  while (paths.size() > 0) {

    auto& path = paths.front();

    auto last = path.back();
    auto lastNode = topo->getNode(last);
    assert(thisNodeType.compare(
				lastNode->getModule()
				->getNedTypeName()) == 0);
	     
    auto lastDistance = lastNode->getDistanceToTarget();

    EV << "EV ShortestPaths " << idStr 
       << ": In getShortestPathsToNode(" << i << ", " << thisTopoId << ")"
       << " Step " << numPathsPopped 
       << ", Now we have " << paths.size() << " in-progress paths "
       << lastDistance << " steps away from " << i
       << ". Current path has last node " << last 
       << "\n.";

    // this path and paths that follow end at target
    if (lastDistance == 0)
      break;

      
    std::map<int, int> closerNeighbors;
    for (int l=0; l< lastNode->getNumOutLinks(); l++) {            
      cTopology::LinkOut* linkOut = lastNode->getLinkOut(l); assert(linkOut);
      cTopology::Node* neighborNode = linkOut->getRemoteNode(); assert(neighborNode);
      std::string neighborNodeType = neighborNode->getModule()->getNedTypeName();
      /*
      EV << "EV ShortestPaths " << idStr
	 << ": neighbor node " << neighborNode->getModule()->getIndex()
	 << " has type " << neighborNodeType << " and is " 
	 << neighborNode->getDistanceToTarget()
	 << " away from target.\n";
      */
      if (neighborNodeType.compare(thisNodeType) != 0) 
	continue;      
	
      auto neighborId = neighborNode->getModule()->getIndex();
      auto neighborDistance = neighborNode->getDistanceToTarget();
      if (neighborDistance == lastDistance-1)
	closerNeighbors[neighborId] = neighborDistance;	
    }
      
    EV << "EV ShortestPaths " << idStr
       << ": Found " << closerNeighbors.size() << " neighbors"
       << " of last node at distance " << lastDistance-1
       << " from target.\n";

    for (const auto& neighbor : closerNeighbors) {
      std::vector<int> newPath(path);
      newPath.push_back(neighbor.first);
      std::stringstream pathSs;
      for (const auto& p : newPath)
	pathSs << p << " ";

      assert(topo->getNode(path.back()) != NULL);

      EV << "EV ShortestPaths " << idStr
	 << ": Added a new path " << pathSs.str() 
	 << " ending in node " << path.back()
	 << " which is at a distance " 
	 << topo->getNode(path.back())->getDistanceToTarget()
	 << " from target.\n ";

      paths.push(newPath);
    }

    paths.pop();
    numPathsPopped++;
  }
  return paths;   
}

  void getRoute(const std::map<int, std::map<int, int> >& nhtables,	\
		const std::map<int, std::map<int, std::vector<int> > >& rtables, \
		const FlowInfo& f,					\
		std::vector<Link>& path) {
 
    int src = f.ends.first;
    int dst = f.ends.second;

    path.clear();
    // TODO(lav): setup routing table and next hop table
    // of all switches

    int prev = -1;
    int next = src;
    
    //path.push_back(next);
    int ingressPort;
    if (prev == -1) ingressPort = -1;
    else {
      assert(nhtables.at(dst).count(src) > 0);
      ingressPort = nhtables.at(dst).at(src);
    }

    int outGateIndex = -1;
    while (next != dst) {

      std::vector<int> candidatePorts;
      assert(rtables.count(next) > 0);
      const auto& it = rtables.at(next).find(dst);
      assert(it != rtables.at(next).end());
      //std::vector<int> ports = it->second;

      std::stringstream portSs;
      for (const auto& port : it->second) {
	if (port != ingressPort) {
	  candidatePorts.push_back(port);
	  portSs << port << " ";
	}
      }

      assert(candidatePorts.size() > 0);

      // TODO(lav): check same random number as in switch..
      int hashval = std::hash<int>()(f.flowId);

      outGateIndex = candidatePorts[(hashval % candidatePorts.size())];

      EV << "GetRoute(nhtables, rtables, FlowInfo(" 
	 << f.flowId << "), path:"
	 << ": prev: " << prev
	 << ", next hop: " << next
	 << ", ingress port was: " << ingressPort
	 << ", dest: " << dst
	 << ", candidate ports: " << portSs.str()
	 << ", hashval % candidatePorts.size(): " 
	 << (hashval % candidatePorts.size())
	 << ", so outGateIndex: " << outGateIndex
	 << ".\n";



      assert(nhtables.at(next).count(outGateIndex) > 0);
      prev = next;
      next = nhtables.at(next).at(outGateIndex);
      path.push_back(Ends(prev,next));
    }    
    return;
 }

} // namespace rcp

