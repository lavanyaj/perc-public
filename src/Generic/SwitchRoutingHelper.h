#ifndef __SWITCH_ROUTING_HELPER_H
#define __SWITCH_ROUTING_HELPER_H

#include <omnetpp.h>
#include "util.h"
#include <map>
#include <queue>
#include <set>

namespace rcp {
  void getRoutingTable(cTopology* topo, int src,			\
		       const std::map<int, int>& nhtableByAddr,		\
		       std::map<int, std::vector<int> >& rtable,\
		       std::string& idStr);

  void getNextHopTable(cTopology* topo, int src,
		       std::map<int, int>& nhtableByPort,
		       std::map<int, int>& nhtableByAddr);

  // nhtables[src][port] :: addr
  // rtables[src][dst] :: std::vector<port>
  void getRoute(const std::map<int, std::map<int, int> >& nhtableByPortBySrc, \
		const std::map<int, std::map<int, std::vector<int> > >& rtableBySrc, \
		const FlowInfo& f,
		std::vector<Link>& path);

std::queue<std::vector<int> >
  getShortestPathsToNode(int i, cTopology* topo, int src,\
			 const std::string& idStr);
} // namespace rcp
#endif // __SWITCH_ROUTING_HELPER_H
