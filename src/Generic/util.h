#ifndef __CHARNY_UTIL_H
#define __CHARNY_UTIL_H

#include <omnetpp.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <sstream>
#include <cfloat> // DBL_MAX;
#include "generic_m.h"
//#include "charny_m.h"
#include "charnymakeflow_m.h"
#include <cmath>
#include <random>
#include <iterator>


namespace rcp { // charny
//#define CHARNY_HDR_BYTES 40  //same as TCP for fair comparison

enum CHARNY_MAKEFLOW_T {CHARNY_MAKEFLOW_START, CHARNY_MAKEFLOW_END,\
    CHARNY_MAKEFLOW_CHANGE_RATE, CHARNY_MAKEFLOW_CHANGE_DEMAND,\
    CHARNY_MAKEFLOW_SYN, CHARNY_MAKEFLOW_FIRST_DATA,\
    CHARNY_MAKEFLOW_FIN, CHARNY_MAKEFLOW_FINACK};

const std::string charnyMakeflowType[] = {"CHARNY_MAKEFLOW_START", "CHARNY_MAKEFLOW_END",\
        "CHARNY_MAKEFLOW_CHANGE_RATE", "CHARNY_MAKEFLOW_CHANGE_DEMAND",\
        "CHARNY_MAKEFLOW_SYN", "CHARNY_MAKEFLOW_FIRST_DATA",\
        "CHARNY_MAKEFLOW_FIN", "CHARNY_MAKEFLOW_FINACK"};

enum CHARNY_PKT_T {CHARNY_HELLO, CHARNY_DATA, CHARNY_FIN};
const std::string charnyPktType[] = {"CHARNY_HELLO", "CHARNY_DATA", "CHARNY_FIN"};
const double INFINITE_RATE = DBL_MAX;
const int K = 1; // K feedback sessions packets per forward session
const double HELLO_INTERVAL = 0.5;

enum CHARNY_PACKET_TYPE {
    CHARNY_DATA_PKT_SYN,
    CHARNY_CONTROL_PKT,
    CHARNY_DATA_PKT

};

const std::string charnyPktTypeStr[] = {"CHARNY_DATA_PKT_SYN", "CHARNY_CONTROL_PKT", "CHARNY_DATA_PKT"};
}; //namespace

namespace rcp { // rcp
typedef std::pair<int, int> pairInt;
typedef std::pair<int, int> Link;
typedef std::pair<int, int> Ends;
typedef std::map<int, int> NextHopTable;
typedef std::map<int, std::vector<int>> RoutingTable; // destaddr -> gateindex

//#define PKT_SIZE 1460

//static const int size_ = PKT_SIZE + RCP_HDR_BYTES;
const double bitsPerByte = 8*1e-6;
enum RCP_PKT_T {RCP_OTHER,
        RCP_SYN,
        RCP_SYNACK,
        RCP_REF,
        RCP_REFACK,
        RCP_DATA,
        RCP_ACK,
        RCP_FIN,
        RCP_FINACK};

const std::string rcpPktTypeStr[] ={"RCP_OTHER",
        "RCP_SYN",
        "RCP_SYNACK",
        "RCP_REF",
        "RCP_REFACK",
        "RCP_DATA",
        "RCP_ACK",
        "RCP_FIN",
        "RCP_FINACK"};

enum RCP_HOST_STATE {RCP_INACT,
             RCP_SYNSENT,
             RCP_CONGEST,
             RCP_RUNNING,
             RCP_RUNNING_WREF,
             RCP_FINSENT,
             RCP_RETRANSMIT,
             RCP_FINTRIGGER}; // if flowGenerator causes fin

const std::string rcpHostStateStr[] = {"RCP_INACT",
        "RCP_SYNSENT",
        "RCP_CONGEST",
        "RCP_RUNNING",
        "RCP_RUNNING_WREF",
        "RCP_FINSENT",
        "RCP_RETRANSMIT",
        "RCP_FINTRIGGER"};

enum RCP_TIMEOUT_STATE { NO_TIMEOUT,
    RCP_TIMEOUT,
    REF_TIMEOUT,
    RTO_TIMEOUT};

const std::string rcpTimeoutStateStr[] = { "NO_TIMEOUT",
        "RCP_TIMEOUT",
        "REF_TIMEOUT",
        "RTO_TIMEOUT"};

enum FLOW_STATE { RTT_, // double
    INTERVAL_, // double
    NUM_SENT_,
    RCP_STATE,
    NUMOUTREFS_,
    MIN_RTT_, //double
    REF_SEQNO_,

    NUM_DATAPKTS_ACKED_BY_RECEIVER_,
    NUM_DATAPKTS_RECEIVED_,
    NUM_PKTS_TO_RETRANSMIT_,
    NUM_PKTS_RESENT_,
    NUM_ENTER_RETRANSMIT_MODE_,

    SEQNO_,
    NUMPKTS_,
    LASTPKTTIME_,

    DESTINATION_
    //INIT_REFINTV_FIX_
    };



class FlowInfo {
public:
    int flowId;
    pairInt ends;
    double duration; // size in flow stats
    double initialRate;
    double startTime; // FIRST_DATA in flow stats
    double endTime; // FIN_ACK in flow stats

    double synTime;
    double finTime;

    // on start: RTTs to converge, after this was added
    // .. its bottleneck number in new setup
    // .. total bottlenecks in new setup - its bottleneck number
    double synWaterfillingInc;
    double synBottleneck;
    double synRtts[5];

    double finWaterfillingInc;
    double finBottleneck;
    double finRtts[5];

    int numFlowsWhenAdded;
    int numFlowsWhenRemoved;

    FlowInfo() : flowId(-1), ends(Ends(-1,-1)), duration(-1), initialRate(-1),\
            startTime(-1), endTime(-1), synTime(-1), finTime(-1), synWaterfillingInc(-1),\
            synBottleneck(-1), finWaterfillingInc(-1), finBottleneck(-1),
            numFlowsWhenAdded(-1), numFlowsWhenRemoved(-1) {
        for (int i = 0; i < 5; i++) {
            synRtts[i] = 0;
            finRtts[i] = 0;
        }
    }

    FlowInfo(int flowId, pairInt ends, double duration, double initialRate, double startTime,\
            double endTime = -1) :\
            flowId(flowId), ends(ends), duration(duration), initialRate(initialRate),\
            startTime(startTime), endTime(endTime), synTime(-1), finTime(-1), synWaterfillingInc(-1),\
            synBottleneck(-1), finWaterfillingInc(-1), finBottleneck(-1){
        for (int i = 0; i < 5; i++) {
                    synRtts[i] = 0;
                    finRtts[i] = 0;
                }
    }
};


bool rateGreaterThan(double rate1, double rate2);

bool rateEqual(double rate1, double rate2);
bool rateWithin(double rate1, double rate2, double eps);

bool rateLessThan(double rate1, double rate2);

bool rateGreaterThanOrEqual(double rate1, double rate2);

bool rateLessThanOrEqual(double rate1, double rate2);

std::string prd(const double x, const int decDigits);

int select_randomly(std::vector<int> *rvec);
}; //namespace

#endif
