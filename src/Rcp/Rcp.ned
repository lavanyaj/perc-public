//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

package src.Rcp;



simple RcpHost
{
    parameters:
        int headerSize;
        int payloadSize;
 
        double SYN_DELAY = default(0.5); // 0.5
    	    double REF_FACT = default(1); //1  //Number of REFs per RTT
        double QUIT_PROB = default(0.1);// 0.1  //probability of quitting if assigned rate is zero
        double REF_INTVAL = default(1);
        
        
        @signal[start](type="long");
        @signal[end](type="long");
        @signal[length](type="int");

        @statistic[flowStart](title="flow start"; source="start"; record=vector,stats; interpolationmode=none);
        @statistic[flowEnd](title="flow end"; source="end"; record=vector,stats; interpolationmode=none);
        @statistic[packetLength](title="packetLength"; source="length"; record=vector,stats; interpolationmode=none);
        
        @signal[*-NumPkts](type=int); // note the wildcard
		@statisticTemplate[sessionNumPkts](record=vector?,stats);
		@signal[*-Rate](type=double); // note the wildcard
		@statisticTemplate[sessionRate](record=vector,stats);
		@signal[*-Rtt](type=double); // note the wildcard
		@statisticTemplate[sessionRtt](record=vector?,stats);
		@signal[*-MinRtt](type=double); // note the wildcard
		@statisticTemplate[sessionMinRtt](record=vector?,stats);
		@signal[*-Interval](type=double); // note the wildcard
		@statisticTemplate[sessionInterval](record=vector?,stats);
		@signal[*-PktType](type=double); // note the wildcard
		@statisticTemplate[sessionPktType](record=vector?,stats);
		@signal[*-HostState](type=double); // note the wildcard
		@statisticTemplate[sessionHostState](record=vector?,stats);
		@signal[*-TimeoutState](type=double); // note the wildcard
		@statisticTemplate[sessionTimeoutState](record=vector?,stats);

		
		
        @display("i=block/routing");
        // int capacity = default(-1);    // negative capacity means unlimited queue
        // bool fifo = default(true);     // whether the module works as a queue (fifo=true) or a stack (fifo=false)

    gates:
        inout gate;
        inout flowAlert;
}

simple RcpSwitch
{
    parameters:
        double initRateFact = default(0.5); // starting rate is 0.5 times link_capacity
        int initNumFlows = default(10); // not used
        double alpha = default(0.5);
        double beta_ = default(0.5);
        double updTimeslot = default(0.01);
        int gamma = default(1);
        double minPprtt = default(0.1);
        double rttGain = default(0.02); // 1/NUMBER OF CO-FLOWS
        double statsTimeoutInterval @unit(s);
        double endTime= default(-1);
        @group(Queueing);
        @display("i=block/queue;q=queue");
        
        @signal[*-arrivalRate](type=double); // note the wildcard
		@statisticTemplate[arrivalRate](record=vector?,stats);
		
		@signal[*-serviceRate](type=double); // note the wildcard
		@statisticTemplate[serviceRate](record=vector?,stats);
		
		@signal[*-serviceRefRate](type=double); // note the wildcard
		@statisticTemplate[serviceRefRate](record=vector?,stats);
		
		@signal[*-arrivalRefRate](type=double); // note the wildcard
		@statisticTemplate[arrivalRefRate](record=vector?,stats);
        
         @signal[*-serviceDataRate](type=double); // note the wildcard
		@statisticTemplate[serviceDataRate](record=vector?,stats);
		
		@signal[*-arrivalDataRate](type=double); // note the wildcard
		@statisticTemplate[arrivalDataRate](record=vector?,stats);
		
		@signal[*-interPacketTime](type=double); // note the wildcard
		@statisticTemplate[interPacketTime](record=vector?,stats);
		
        @signal[*-queueLength](type=int); // note the wildcard
		@statisticTemplate[queueLength](record=vector?,stats,histogram);
        @signal[*-queueByteLength](type=int); // note the wildcard
		@statisticTemplate[queueByteLength](record=vector?,stats,histogram);
        @signal[*-flowRate](type=double); // note the wildcard
		@statisticTemplate[flowRate](record=vector?,stats,histogram);
        @signal[*-Rtt](type=double); // note the wildcard
		@statisticTemplate[Rtt](record=vector?,stats,histogram);
		@signal[*-AvailableRate](type=double);
		@statisticTemplate[AvailableRate](record=vector?,stats,histogram);
		@signal[*-QueueDrain](type=double);
		@statisticTemplate[QueueDrain](record=vector?,stats,histogram);
		@signal[*-InputTraffic](type=double);
		@statisticTemplate[InputTraffic](record=vector?,stats,histogram);
        
        
    gates:
        inout local;
        inout port[];
        output ingress_end[];
        input egress_start[];
}


module RcpNode
{
    parameters:
        @display("i=misc/node_vs,gold");
    gates:
        inout port[];
        inout flowAlert;
    submodules:
        //dropTailQueue: DropTailQueue;
        rcpSwitch: RcpSwitch;
        rcpHost: RcpHost;
    connections:
		flowAlert <--> rcpHost.flowAlert;
		rcpHost.gate <--> rcpSwitch.local;
		
        //port++ <--> rcpSwitch.port++;
        for i=0..sizeof(port)-1 {
            port[i] <--> rcpSwitch.port++;
        }
}

