#include "RobustFrugalHost.h"
#include <assert.h>

namespace rcp {
Define_Module(RobustFrugalHost);


#ifndef EVH
#define EVH if (false) EV << "EVH: "
#ifndef EV3
#define EV3 if (false) std::cout
#ifndef EV2
#define EV2 if (false) std::cout

void RobustFrugalHost::updateProtocolSpecificControlPacket(GenericControlPacket* msg) {

  RobustFrugalPacket *pkt = check_and_cast<RobustFrugalPacket *>(msg);
  const GenericControlHostSessionState& genericSession = pIActiveSessions.at(pkt->getFlowId());

  int maxHops = MAX_HOPS;
  if (genericSession.maxHopsFromRxPkt > 0) maxHops = genericSession.maxHopsFromRxPkt;

  if (!pkt->getIsForward() and pkt->getIsExit()) {    
    for (int hop = 0; hop < maxHops; hop++)
      pkt->setNewLabel(hop, RobustFrugalSwitchSessionStateRates::LABEL_UNDEF);
    addTpp(pkt, maxHops);
  }

    EVH << "RobustFrugalHost "
	<< "address(" << address << ")"
	<< ", in function updateProtocolSpecifciControlPacket" 
	<< ": updating packet for flow " << pkt->getFlowId()
	<< ": " << getString(pkt, maxHops) << ".\n";

    EV << "EVENT! RobustControlHost "
       << "(address: " << address << ")"
       << ", seen leaving "
       << (genericSession.isForward ? " source" : " destination")
       << ", packet for flow " << pkt->getFlowId()
       << ": " << getString(pkt, maxHops) << "."
       << "\n";

}				    

  GenericControlPacket* RobustFrugalHost::newProtocolSpecificControlPacket
  (const char* pktname, int flowId) {
    RobustFrugalPacket* pkt = new RobustFrugalPacket(pktname);
    const GenericControlHostSessionState& genericSession = pIActiveSessions.at(flowId);
    RobustFrugalHostSessionState &sMSession = sMActiveSessions.at(flowId);

    int maxHops = MAX_HOPS;
    if (genericSession.maxHopsFromRxPkt > 0) maxHops = genericSession.maxHopsFromRxPkt;

    for (int hop = 0; hop < maxHops; hop++) {
        pkt->setSumSat(hop, -1);
        pkt->setNumSat(hop, -1);
        pkt->setNumUnsat(hop, -1);
        pkt->setCapacity(hop, -1);

        pkt->setNewRate(hop, sMSession.newDemand[hop]);
        pkt->setNewLabel(hop, sMSession.newLabel[hop]);
	//assert(sMSession.newLabel[hop] == RobustFrugalSwitchSessionStateRates::LABEL_UNSAT);

        pkt->setOldRate(hop, sMSession.oldDemand[hop]);
        pkt->setOldLabel(hop, sMSession.oldLabel[hop]);
	//assert(sMSession.oldLabel[hop] == RobustFrugalSwitchSessionStateRates::LABEL_UNDEF);

    }

    addTpp(pkt, maxHops);

    EVH << "RobustFrugalHost "
	<< "address(" << address << ")"
	<< ", in function newProtocolSpecificControlPacket"
	<< ": updating packet for flow " << pkt->getFlowId()
	<< ": " << getString(pkt, maxHops) << ".\n";

    //pkt->setMaxHops(sMSession.maxHops);
    //pkt->setMaxHops(0);
    return pkt;
}

  void RobustFrugalHost::protocolSpecificSetupSession(const CharnyMakeFlowMsg *msg) {
						       
    int flowId = msg->getFlowId();
    GenericControlHostSessionState& genericSession = pIActiveSessions.at(flowId);
    RobustFrugalHostSessionState session(flowId, true); // source=true
    // at source (newLabel: unsat, oldLabel: undef)
    sMActiveSessions[flowId] = session;   
    // only state- newdemand, newlabel, old..
    // and linkState
    int maxHops = MAX_HOPS;
    if (genericSession.maxHopsFromRxPkt > 0) maxHops = genericSession.maxHopsFromRxPkt;

    for (int hop = 0; hop < maxHops; hop++) {
      assert(session.newDemand[hop] == INFINITE_RATE);
      assert(session.newLabel[hop] == RobustFrugalSwitchSessionStateRates::LABEL_UNSAT);
      assert(session.oldDemand[hop] == INFINITE_RATE);
      assert(session.oldLabel[hop] == RobustFrugalSwitchSessionStateRates::LABEL_UNDEF);
      assert(session.linkState.linkCapacity == 100000000000);
    }

}

  // adds a TPP to program : i.e., addToReg(xx) etc.
  // for each hop
  void RobustFrugalHost::addTpp(GenericControlPacket *msg, int maxHops) {
  RobustFrugalPacket *pkt = check_and_cast<RobustFrugalPacket *>(msg);
  
  for (int hop = 0; hop < maxHops; hop++) {

    // actually changes to agg.
    double sumSat = 0;
    double numSat = 0;
    double numUnsat = 0;

    int oldLabel = pkt->getOldLabel(hop);
    int newLabel = pkt->getNewLabel(hop);
    double oldRate = pkt->getOldRate(hop);
    double newRate = pkt->getNewRate(hop);

    // SAT to SAT
    if (oldLabel == RobustFrugalSwitchSessionStateRates::LABEL_SAT
	&& newLabel == RobustFrugalSwitchSessionStateRates::LABEL_SAT) {
      sumSat = sumSat - oldRate + newRate;
    } else if (oldLabel == RobustFrugalSwitchSessionStateRates::LABEL_UNSAT
	       && newLabel == RobustFrugalSwitchSessionStateRates::LABEL_SAT) {
      // Session changed from UNSAT, old rate to SAT, new rate
      numUnsat--;
      numSat++;
      sumSat = sumSat + newRate;
    } else if (oldLabel == RobustFrugalSwitchSessionStateRates::LABEL_UNSAT
	       && newLabel == RobustFrugalSwitchSessionStateRates::LABEL_UNSAT) {
        // Session changed from UNSAT, old rate to UNSAT, new rate
        // no change to sumSat
    } else if (oldLabel == RobustFrugalSwitchSessionStateRates::LABEL_SAT
            && newLabel == RobustFrugalSwitchSessionStateRates::LABEL_UNSAT) {
      // Session changed from SAT, old rate to UNSAT, new rate
        numUnsat++;
        numSat--;
        sumSat -= oldRate;
        if (rateEqual(sumSat, 0)) {sumSat = 0;}
    } else if (oldLabel == RobustFrugalSwitchSessionStateRates::LABEL_UNDEF
	       && newLabel == RobustFrugalSwitchSessionStateRates::LABEL_SAT) {
      numSat++;
      sumSat = sumSat + newRate;
    } else if (oldLabel == RobustFrugalSwitchSessionStateRates::LABEL_UNDEF
	       && newLabel == RobustFrugalSwitchSessionStateRates::LABEL_UNSAT) {
      numUnsat++;
      EV << "Old label: " << oldLabel << "\n New label: " << newLabel
	 << "\n numUnsat = " << numUnsat << endl;
    } else if (newLabel == RobustFrugalSwitchSessionStateRates::LABEL_UNDEF
	       && oldLabel == RobustFrugalSwitchSessionStateRates::LABEL_UNSAT) {
      // Unsat session exiting (new rate undefined)
      numUnsat--;
      EV << "Old label: " << oldLabel << "\n New label: " << newLabel
	 << "\n numUnsat = " << numUnsat << endl;
    } else if (newLabel == RobustFrugalSwitchSessionStateRates::LABEL_UNDEF
	       && oldLabel == RobustFrugalSwitchSessionStateRates::LABEL_SAT) {
      // Sat session exiting (new rate undefined)
      numSat--;
      sumSat -= oldRate;
    } else {
      EV << "FATAL!!! Invalid: Old label: " << oldLabel
	  << ", new label: " << newLabel << "\n";
    }
    
    pkt->setAddToSumSat(hop, sumSat);
    pkt->setAddToNumSat(hop, numSat);
    pkt->setAddToNumUnsat(hop, numUnsat);
  } // for (int hop ..
}

  void RobustFrugalHost::sourceReceivePacket(const GenericControlPacket *msg) {
    const RobustFrugalPacket *pkt = check_and_cast<RobustFrugalPacket *>(msg);
    GenericControlHostSessionState &genericSession = pIActiveSessions.at(pkt->getFlowId());
    genericSession.numRecvd++;

    if (pkt->getPktType() == CHARNY_FIN) {
        EV3 << "EVENT! Source received FIN for flow " << pkt->getFlowId() << ", do nothing.\n";
        // since switches don't really add any info. to packets, just delete flow from state.
        return;
    }

    int maxHops = MAX_HOPS;
    // maxhopsFromRxPkt <- pkt->getHop() or pkt->getMaxHop() by GenericControlHost firt
    if (genericSession.maxHopsFromRxPkt > 0) maxHops = genericSession.maxHopsFromRxPkt;

    EVH << "sourceReceivePacket "
	<< " address (" << address << "): "
	<< "from flow " << pkt->getFlowId()
	<< ": " << getString(pkt, maxHops)
                 << "\n";

    sourceOrDestinationReceivePacket(msg);

    //emitSignal(pkt->getFlowId(), "StampedRate", getIncomingRateFromId(pkt->getFlowId()));
        EVH << "At time "<< simTime() << ", emitting sourceReceivePacketSignal: " \
                  << getIncomingRateFromId(pkt->getFlowId()) << ".\n";

    updateTransmissionRate(msg);

}

void RobustFrugalHost::sourceReceivePacketGeneral(const GenericControlPacket *msg) {
    sourceReceivePacket(msg);
}

double RobustFrugalHost::getIncomingRateFromId(int flowId) {
    assert(sMActiveSessions.count(flowId) > 0);
    return sMActiveSessions.at(flowId).incomingRate;
}

  void RobustFrugalHost::sourceOrDestinationReceivePacket(const GenericControlPacket *msg) {
    const RobustFrugalPacket *pkt = check_and_cast<RobustFrugalPacket *>(msg);
  
    const GenericControlHostSessionState& genericSession = pIActiveSessions.at(pkt->getFlowId());
    RobustFrugalHostSessionState &sMSession = sMActiveSessions.at(pkt->getFlowId());

    // Get smallest bottleneck rates
    std::vector<RobustFrugalLinkInfo> info;
    double smallest = INFINITE_RATE;
    double second_smallest = INFINITE_RATE;
    std::vector<RobustFrugalLinkInfo> linkInfo;
    
    int maxHops = MAX_HOPS;
    // maxhopsFromRxPkt <- pkt->getHop() or pkt->getMaxHop() by GenericControlHost firt
    if (genericSession.maxHopsFromRxPkt > 0) maxHops = genericSession.maxHopsFromRxPkt;

    for (int hop = 0; hop < maxHops; hop++) {
      RobustFrugalLinkInfo l(pkt, hop);
      EVH << "Doing link local computation for hop " << hop << "\n";
      linkLocalComputation(l);
      EVH << "Rate at hop " << hop << " is tmpB = " << l.tmpB
	  << " for capacity = " << l.capacity <<
	", sumSat = " << l.sumSat
	  << ", numUnsat = " << l.numUnsat
	  << ", numSat = " << l.numSat << "\n";

      if (rateLessThan(l.tmpB, smallest) && rateLessThan(l.tmpB, second_smallest)) {
	second_smallest = smallest;
	smallest = l.tmpB;
      } else if (rateLessThan(l.tmpB, second_smallest))
	second_smallest = l.tmpB;
      linkInfo.push_back(l);
    }

    EVH << "Smallest Rates are " << smallest << " and second_smallest "
	<< second_smallest << "\n";

    std::stringstream flowStateStr;
    flowStateStr << "Flow " << pkt->getFlowId()
		 << "-From" << pkt->getSource()
		 << "-To" << pkt->getDestination();

    // TODO(lav): Hosts/ flows have no idea of switch ID!?
    // only hops.

    // Update demand and labels
    for (int hop = 0; hop < maxHops; hop++) {
      auto& l = linkInfo.at(hop);
      assert(rateLessThanOrEqual(smallest, l.tmpB));
      if (rateEqual(l.tmpB, smallest)) {
	// mark as unsaturated at smallest link
	l.newRate = second_smallest;
	l.newLabel = RobustFrugalSwitchSessionStateRates::LABEL_UNSAT;
	flowStateStr << " UNSAT@Hop" << hop << ": " << smallest;
      } else {
	// mark as saturated (elsewhere) at other links	   
	l.newRate = smallest;
	l.newLabel = RobustFrugalSwitchSessionStateRates::LABEL_SAT;
	flowStateStr << " SAT@Hop" << hop;
      }
      // the newRate in the received packet becomes the oldRate in the new packet
      l.oldRate = l.rate;
      l.oldLabel = l.label;
    }

    EV << "FLOW-STATE " << flowStateStr.str() << std::endl;
    getParentModule()->bubble(flowStateStr.str().c_str());

    std::stringstream hostUpdateStr;
    hostUpdateStr << "RobustFrugalHost "
		  << " (address " << address << ")"
		  << ", in function sourceOrDestinationReceivePacket"
		  << ": updating host state demand/labels "
		  << " on receiving packet for flow  " << pkt->getFlowId()
		  << ": " << getString(pkt, maxHops) << ".\n";

    // update demands and labels, about indexing 
    // first link on forward path updates hop=0 on both forward and reverse path
    for (int hop = 0; hop < maxHops; hop++) {
      int incomingHop = hop; //maxHops - hop - 1;
      auto& l = linkInfo.at(incomingHop);
      sMSession.newDemand[hop] = l.newRate;
      sMSession.newLabel[hop] = l.newLabel;
      sMSession.oldDemand[hop] = l.oldRate;
      sMSession.oldLabel[hop] = l.oldLabel;
      hostUpdateStr << "Updated demand, label of flow "
		    << pkt->getFlowId() << " at hop "
		    << hop << " to " 
		    << "newDemand " << sMSession.newDemand[hop]
		    << ", newLabel " 
		    << RobustFrugalSwitchSessionStateRates::labelStr(sMSession.newLabel[hop])
		    << ", oldDemand " << sMSession.oldDemand[hop]
		    << ", oldLabel " 
		    << RobustFrugalSwitchSessionStateRates::labelStr(sMSession.oldLabel[hop])
		    << ". ";
    }
    sMSession.incomingRate = smallest;
    EV << hostUpdateStr.str();
  }


bool RobustFrugalHost::destinationReceivePacket(const GenericControlPacket *msg) {
    const RobustFrugalPacket *pkt = check_and_cast<RobustFrugalPacket *>(msg);
    RobustFrugalHostSessionState &session = sMActiveSessions.at(pkt->getFlowId());
    const GenericControlHostSessionState& genericSession = pIActiveSessions.at(pkt->getFlowId());

    int maxHops = MAX_HOPS;
    // maxhopsFromRxPkt <- pkt->getHop() or pkt->getMaxHop() by GenericControlHost firt
    if (genericSession.maxHopsFromRxPkt > 0) maxHops = genericSession.maxHopsFromRxPkt;

    EVH << "destinationReceivePacket "
	<< "address (" << address << "): "
	<< "from flow " << pkt->getFlowId()
	<< ": " << getString(pkt, maxHops) << ".\n";

    sourceOrDestinationReceivePacket(msg);


    //emitSignal(pkt->getFlowId(), "StampedRate", getIncomingRateFromId(pkt->getFlowId()));
    // EVH << "At time "<< simTime() << ", emitting destinationReceivePacketSignal: "\
    //               << getIncomingRateFromId(pkt->getFlowId()) << ".\n";

    return true;
}

void RobustFrugalHost::setLinkCapacity(int flowId, double cap) {
    sMActiveSessions.at(flowId).linkState.linkCapacity = cap;
}

double RobustFrugalHost::getLinkCapacity(int flowId) {
    assert(sMActiveSessions.count(flowId) > 0);
    return sMActiveSessions.at(flowId).linkState.linkCapacity;
}

void RobustFrugalHost::protocolSpecificProcessIngress(GenericControlPacket *msg) {
    EVH << "In RobustFrugalHost::protocolSpecificProcessIngress for msg for flow " << msg->getFlowId() << "\n";
  // TODO(lav): OK that it's only done at source??
    RobustFrugalPacket *pkt = check_and_cast<RobustFrugalPacket *>(msg);
    if (!pkt->getIsForward()) {
        assert(pIActiveSessions.count(pkt->getFlowId())>0);
        assert(sMActiveSessions.count(pkt->getFlowId())>0);
        EVH << "Calling linkState.processIngress.\n";
        sMActiveSessions.at(pkt->getFlowId()).linkState.processIngress(pkt);

	EV << "EVENT! RobustFrugalLinkState (Ingress) "
	   << "(address: " <<  sMActiveSessions.at(pkt->getFlowId()).linkState.getId() << ")"
	   << ", seen leaving link,"
	   << " packet for flow " << pkt->getFlowId()
	   << ": " << getString(pkt) << "."
	   << "\n";

    } // may not have session state for flows whose forward packets enter at ingress
    // (don't need to either cuz that link sharing is managed by switch on other end of link)

}

void RobustFrugalHost::protocolSpecificProcessEgress(GenericControlPacket *msg) {
    RobustFrugalPacket *pkt = check_and_cast<RobustFrugalPacket *>(msg);
    int flowId = pkt->getFlowId();
    sMActiveSessions.at(flowId).linkState.processEgress(pkt);

    EV << "EVENT! RobustFrugalLinkState (Egress) "
       << "(address: " <<  sMActiveSessions.at(pkt->getFlowId()).linkState.getId() << ")"
       << ", seen leaving link,"
       << " packet for flow " << pkt->getFlowId()
       << ": " << getString(pkt) << "."
       << "\n";

}

void RobustFrugalHost::addProtocolSpecificActiveSession(const GenericControlPacket *pkt) {
    int flowId = pkt->getFlowId();
    RobustFrugalHostSessionState session(flowId, false); // source=false
    // at destination, new label unsat, old label undef)
    // but updated according to received packet?
    // on destinationReceivePacket
    sMActiveSessions[flowId] = session;
}; // at destination

void RobustFrugalHost::removeProtocolSpecificActiveSession(int flowId) {\
    sMActiveSessions.erase(flowId);
}; // on FIN


  // TODO(lav): Stays the same
void RobustFrugalHost::linkLocalComputation(RobustFrugalLinkInfo& l) {
    RobustFrugalLinkInfo old(l);

    // treat flow as unsaturated
    if (l.label == RobustFrugalSwitchSessionStateRates::LABEL_UNDEF) {
        l.numUnsat++; l.label = RobustFrugalSwitchSessionStateRates::LABEL_UNDEF;
    } else if (l.label == RobustFrugalSwitchSessionStateRates::LABEL_SAT) {
        l.sumSat -= l.rate; l.numUnsat++;
        l.label = RobustFrugalSwitchSessionStateRates::LABEL_UNDEF;
    }

    EV << "l.numUnsat = " << l.numUnsat << endl;
    EV << "l.sumSat = " << l.sumSat << endl;
    EV << "l.label = " << l.label << endl;

    // get rates from other links
    // because we treat this flow as unsaturated numSat should be at least 1
    // sumSat can't be negative
    assert (l.numUnsat >= 1 && rateGreaterThanOrEqual(l.sumSat, 0));
    double tmpR =  ((l.capacity - l.sumSat)/l.numUnsat);

    double avgSat = l.sumSat/l.numSat;

    double tmpB = std::max(tmpR, avgSat);
    l = old;
    l.tmpB = tmpB;

    return;
}





#endif // EV2
#endif // EV3
#endif // EVH
}; // namespace
