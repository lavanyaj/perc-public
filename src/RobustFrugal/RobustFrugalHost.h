//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __ROBUSTFRUGAL_HOST_H
#define __ROBUSTFRUGAL_HOST_H

#include <omnetpp.h>
#include "util.h"
#include "GenericControlHost.h"
#include "RobustFrugalSwitch.h"
#include "robustfrugal_m.h"
#include <map>
#include <utility>
#include <assert.h>

namespace rcp {


/*
 * Per session state at link
 *  flowId
 *  maxHops maybe not needed
 *  bottleneck rate and hop
 */
class RobustFrugalHostSessionState : public GenericControlHostSessionState {
public:
    int flowId;

    double newDemand[MAX_HOPS];
    int newLabel[MAX_HOPS];
    double oldDemand[MAX_HOPS];
    int oldLabel[MAX_HOPS];

    double incomingRate;
    /*
    static const int LABEL_SAT = 0;
    static const int LABEL_UNSAT = 1;
    static const int LABEL_UNDEF = -1;
    */
    RobustFrugalLinkState linkState;
    RobustFrugalHostSessionState() :
    flowId(-1), linkState("", NULL), incomingRate(-1) {
            assert(MAX_HOPS <= 10);
            for (int hop = 0; hop < MAX_HOPS; hop++) {
               newDemand[hop] = INFINITE_RATE;
               newLabel[hop] = RobustFrugalSwitchSessionStateRates::LABEL_UNSAT;

               // are these the correct values to set for the old demand/label?
               oldDemand[hop] = INFINITE_RATE;
               oldLabel[hop] = RobustFrugalSwitchSessionStateRates::LABEL_UNDEF;
            }

            linkState.linkCapacity = 100000000000;
        }

    RobustFrugalHostSessionState(int fid, bool src) :
    flowId(fid), linkState("", NULL), incomingRate(-1) {
        std::stringstream ss;
        if (src)
            ss << "Source-For-Flow-" << fid;
        else ss << "Dest-For-Flow-" << fid;
        linkState = RobustFrugalLinkState(ss.str(), NULL);

        assert(MAX_HOPS <= 10);
        for (int hop = 0; hop < MAX_HOPS; hop++) {
            newDemand[hop] = INFINITE_RATE;
            newLabel[hop] = RobustFrugalSwitchSessionStateRates::LABEL_UNSAT;

            // are these the correct values to set for the old demand/label?
            oldDemand[hop] = INFINITE_RATE;
            oldLabel[hop] = RobustFrugalSwitchSessionStateRates::LABEL_UNDEF;
        }
        linkState.linkCapacity = 100000000000;
	}
 
  };

class RobustFrugalLinkInfo {
public:
    int numUnsat;
    double sumSat;
    double numSat;
    double capacity;
    double rate;
    int label;
    double newRate;
    int newLabel;
    double oldRate;
    int oldLabel;
    double tmpB;
    RobustFrugalLinkInfo(const RobustFrugalPacket *pkt, int hop) {
        numUnsat = pkt->getNumUnsat(hop);
        sumSat = pkt->getSumSat(hop);
        numSat = pkt->getNumSat(hop);
        capacity = pkt->getCapacity(hop);
        rate = pkt->getNewRate(hop);
        label = pkt->getNewLabel(hop);

        tmpB = -1;
        newRate = -1;
        newLabel = RobustFrugalSwitchSessionStateRates::LABEL_UNDEF;
        oldRate = -1;
        oldLabel = RobustFrugalSwitchSessionStateRates::LABEL_UNDEF;
    }
};

class RobustFrugalHost : public GenericControlHost
{

  private:
     // RobustFrugal per flow state at host
    typedef std::map<int, RobustFrugalHostSessionState> SessionTable;
    SessionTable sMActiveSessions;

  protected:
    virtual GenericControlPacket *newProtocolSpecificControlPacket(const char* pktname, int flowId);
    virtual void updateProtocolSpecificControlPacket(GenericControlPacket* pkt);
    void addTpp(GenericControlPacket *msg, int maxHops);

    virtual void sourceReceivePacket(const GenericControlPacket *pkt); // assumes demand is always INFINITE
    virtual void sourceReceivePacketGeneral(const GenericControlPacket *pkt); // demand could be FINITE, changing
    virtual bool destinationReceivePacket(const GenericControlPacket *pkt);
    virtual void sourceOrDestinationReceivePacket(const GenericControlPacket *msg);

    //virtual void updateTransmissionRate(const GenericControlPacket *pkt);
    virtual double getIncomingRateFromId(int flowId);

    //virtual void egressAction(GenericControlPacket *msg);
    virtual void protocolSpecificProcessIngress(GenericControlPacket *msg);
    virtual void protocolSpecificProcessEgress(GenericControlPacket *msg);
    virtual void setLinkCapacity(int flowId, double cap);
    virtual double getLinkCapacity(int flowId);

    virtual void linkLocalComputation(RobustFrugalLinkInfo& l);


    virtual void protocolSpecificSetupSession(const CharnyMakeFlowMsg *msg); // at source
    virtual void addProtocolSpecificActiveSession(const GenericControlPacket *pkt); // at destination
    virtual void removeProtocolSpecificActiveSession(int flowId); // on FIN


};


}; // namespace

#endif // __ROBUSTFRUGAL_HOST_H
