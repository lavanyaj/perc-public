#include "RobustFrugalSwitch.h"

#include <assert.h>
#include <algorithm>
namespace rcp {

Define_Module(RobustFrugalSwitch);

#ifndef EVG
#define EVG  EV << "EVG: " //if (false) std::cout
#ifndef EV2
#define EV2 if (false) std::cout
#ifndef EV1
#define EV1 if (false) std::cout

std::string getString(const RobustFrugalPacket *pkt, int maxHops) {
    std::stringstream ss;
    //int maxHops = std::max(pkt->getHop()+1, pkt->getMaxHops());
    //if (maxHops == -1) maxHops = MAX_HOPS;
    if (pkt->getIsForward())
      ss << "Packet on forward path .";
    else 
      ss << "Packet on reverse path.";

    if (pkt->getIsExit()) 
      ss << " Exiting. ";
    else 
      ss << " Not exiting yet. ";
    
    ss << "Note isForward, isExit initialized a lil after others. ";

    // If maxHops not specified print only one hop
    int startHop = 0;
    if (maxHops == -1) {
      startHop = pkt->getHop();
      maxHops = startHop+1;
    }

    for (int hop = 0; hop < maxHops; hop++) {
        ss << "hop: " << hop;
        ss << ", newRate: " << pkt->getNewRate(hop);
        ss << ", newLabel: " 
	   << RobustFrugalSwitchSessionStateRates::labelStr(pkt->getNewLabel(hop));
        ss << ", oldRate: " << pkt->getOldRate(hop);
        ss << ", oldLabel: " 
	   << RobustFrugalSwitchSessionStateRates::labelStr(pkt->getOldLabel(hop));
	ss << ", change to sumSat: " << pkt->getAddToSumSat(hop);
	ss << ", change to numSat: " << pkt->getAddToNumSat(hop);
	ss << ", change to numUnsat: " << pkt->getAddToNumUnsat(hop);
	
        double sumSat[MAX_HOPS];
            double numSat[MAX_HOPS];
            double numUnsat[MAX_HOPS];
            double capacity[MAX_HOPS];
        ss << ", State read at switch:"
	   << " R = (" << pkt->getCapacity(hop)
                << " - " << pkt->getSumSat(hop)
                << ")/ " << pkt->getNumUnsat(hop);
        ss << ", B = max(R, " << pkt->getSumSat(hop) << "/" << pkt->getNumSat(hop) << ")";
        ss << ". ";        
    }


    return ss.str();
}

RobustFrugalLinkState::~RobustFrugalLinkState() {
}

RobustFrugalSwitch::RobustFrugalSwitch() {
}

RobustFrugalSwitch::~RobustFrugalSwitch() {



}

void RobustFrugalSwitch::finish() {
    EVG << "Clearing Link State for RobustFrugal.\n";
    for (auto &ls : cltable) {
        delete ls.second;
        ls.second = NULL;
    }
    cltable.clear();
    GenericSwitch::finish();
}
void RobustFrugalSwitch::initialize()
{
    GenericSwitch::initialize();
   for (const auto &linkState: ltable) {
     cltable[linkState.first] = new RobustFrugalLinkState(linkState.second->getId(), this);
        EVG << "Added new RobustFrugal link state for link " << linkState.first\
                << ".\n";

        cltable[linkState.first]->linkCapacity = linkState.second->linkCapacity;
        cltable[linkState.first]->header_size_ = par("headerSize").doubleValue();
        cltable[linkState.first]->payload_size_ = par("payloadSize").doubleValue();
        cltable[linkState.first]->minRtt = par("minRtt").doubleValue();
        cltable[linkState.first]->maxSessionsPerRtt = par("maxSessionsPerRtt").doubleValue();

    }
}

cMessage* RobustFrugalSwitch::protocolSpecificProcessEgress(int outGateIndex, cMessage* msg) {
    EVG << "Calling cltable.at("<< outGateIndex << ")->processEgress\n";
    RobustFrugalPacket * pkt = check_and_cast<RobustFrugalPacket *>(msg);

    cMessage* ret = cltable.at(outGateIndex)->processEgress(pkt);

    RobustFrugalPacket* retpkt = check_and_cast<RobustFrugalPacket *>(ret);
    EV << "EVENT! RobustFrugalLinkState (Egress) "
       << "(address: " <<  cltable.at(outGateIndex)->getId() << ")"
       << ", seen leaving link,"
       << " packet for flow " << retpkt->getFlowId()
       << ": " << getString(retpkt) << "."
       << "\n";
    return ret;

    //return cltable.at(outGateIndex)->processEgress(pkt);

}

// actually ingate index etc.
cMessage* RobustFrugalSwitch::protocolSpecificProcessIngress(int outGateIndex, cMessage* msg) {
  cMessage* ret = cltable.at(outGateIndex)->processIngress(check_and_cast<RobustFrugalPacket *>(msg));

    RobustFrugalPacket* retpkt = check_and_cast<RobustFrugalPacket *>(ret);
    EV << "EVENT! RobustFrugalLinkState (Ingress) "
       << "(address: " <<  cltable.at(outGateIndex)->getId() << ")"
       << ", seen leaving link,"
       << " packet for flow " << retpkt->getFlowId()
       << ": " << getString(retpkt) << "."
       << "\n";
    return ret;
}

RobustFrugalPacket *RobustFrugalLinkState::processIngress(RobustFrugalPacket *pkt)
{
    assert(pkt->getPktType() == CHARNY_HELLO || pkt->getPktType() == CHARNY_FIN);
    if(pkt->getIsForward()) return pkt;
    processIngressOrEgress(pkt, false);
    return pkt;
}

RobustFrugalPacket *RobustFrugalLinkState::processEgress(RobustFrugalPacket *pkt) {
    if (!pkt->getIsForward()) return pkt;
    processIngressOrEgress(pkt, true);
    return pkt;
}

RobustFrugalPacket *RobustFrugalLinkState::processIngressOrEgress(RobustFrugalPacket *pkt, bool egress)
{
  // TODO(lav): Replace with functions that simply
  // look at "register changes" fields and updates registers
  // as long as valid packet at xgress, only need to apply register
  // changes- for new, existing, exiting flows

    assert(pkt->getPktType() == CHARNY_HELLO || pkt->getPktType() == CHARNY_FIN);
    assert((egress && pkt->getIsForward()) || (!egress && !pkt->getIsForward()));

    // Change to exit processing only on ingress packets (reverse)
    // commented out to always process if (!(egress && pkt->getIsExit()))
    localComputationKnown(pkt, egress);
    forwardingActionKnown(pkt);

    std::stringstream switchStateStr;
    switchStateStr << "SWITCH-STATE";
    switchStateStr << " switch-link: " << this->getId();
    switchStateStr << " sumSat: " << sumSat
		   << " numSat: " << numSat
		   << " numUnsat: " << numUnsat;
    switchStateStr << " lastPacket: ";
    if (egress) switchStateStr << "EGRESS";
    else switchStateStr << "INGRESS";
    if (pkt->getIsForward()) switchStateStr << "-FWD"; 
    else switchStateStr << "-REV";
    if (!pkt->getIsExit()) switchStateStr << "-REG";
    else switchStateStr << "-EXIT";
    switchStateStr << "-Flow" 
		   << pkt->getFlowId() 
		   << "-From" 
		   << pkt->getSource()
		   << "-To"
		   << pkt->getDestination();

    EV << switchStateStr.str() << std::endl;

    if (modulePtr) modulePtr->getParentModule()->bubble(switchStateStr.str().c_str());
    return pkt;
}


void RobustFrugalLinkState::localComputationKnown(const RobustFrugalPacket *pkt, bool egress) {
  std::stringstream switchUpdateStr;
  double oldSumSat = sumSat;
  double oldNumSat = numSat;
  double oldNumUnsat = numUnsat;

  assert(pkt->getHop() >= 0);
  int hop = pkt->getHop();
    
  if (pkt->getAddToSumSat(hop) != 0) {
    sumSat += pkt->getAddToSumSat(hop);
    // floating point precision..
    if (rateEqual(sumSat, 0)) {sumSat = 0;}

    switchUpdateStr << "sumSat = " 
		    << oldSumSat 
		    << " (old value)"
		    << " + " << pkt->getAddToSumSat(hop) 
		    << " (from packet) = "
		    << sumSat
		    << " (new value), ";

    assert(rateGreaterThanOrEqual(sumSat, 0));
  }

    if (pkt->getAddToNumSat(hop) != 0) {
      numSat += pkt->getAddToNumSat(hop);
      switchUpdateStr << "numSat = " 
		      << oldNumSat 
		      << " (old value)"
		      << " + " << pkt->getAddToNumSat(hop) 
		      << " (from packet) = "
		      << numSat
		      << " (new value), ";

      assert(numSat >= 0);
    }
    
    if (pkt->getAddToNumUnsat(hop) != 0) {
      numUnsat += pkt->getAddToNumUnsat(hop);
      switchUpdateStr << "numUnsat = " 
		      << oldNumUnsat 
		      << " (old value)"
		      << " + " << pkt->getAddToNumUnsat(hop) 
		      << " (from packet) = "
		      << numUnsat
		      << " (new value), ";

      assert(numUnsat >= 0);
      // all flows could be saturated
      // so that numUnsat = 0
      // when we compute fair share for
      // a flow (at the end host)
      // we assume that particular
      // flow is unsat with demand inf.
    }
    EV << "RobustFrugalLinkState  "
       << "(address: " << this->getId() << ")"
       << ", in function localComputationKnown(..,egress=" << egress << ")"
       << ": updating local state on seeing packet for flow " << pkt->getFlowId()
       << ": " << getString(pkt, hop) << "."
       << " Switch updates are  " 
       << switchUpdateStr.str()
       << "\n";

    //assert(numSat >= 0);
    //assert(rateGreaterThan(sumSat, 0));
    //assert(numUnsat >= 0);
}

void RobustFrugalLinkState::forwardingActionKnown(RobustFrugalPacket *pkt) {

    // using avg sat but that is computed by the end hosts

    assert(pkt->getHop() >= 0);
    int hop = pkt->getHop();

    pkt->setSumSat(hop, sumSat);
    pkt->setNumSat(hop, numSat);
    pkt->setNumUnsat(hop, numUnsat);
    pkt->setCapacity(hop, getAvailableLinkCapacity());

    if (pkt->getIsForward())
        EVG << "Link " << this->getId()
            << " set for hop " << hop
            << " of forward"
            << " packet: " << getString(pkt, hop) << "\n";
    else
        EVG << "Link " << this->getId()
        << " set for hop " << hop
        << " of reverse"
        << " packet: " << getString(pkt, hop) << "\n";

    EV << "RobustFrugalLinkState  "
       << "(address: " << this->getId() << ")"
       << ", in function forwardingActionKnown"
       << ": updating packet (copy local aggregates)  for flow " << pkt->getFlowId()
       << ": " << getString(pkt, hop) << "."
       << "\n";

}



#endif // EV1
#endif // EV2
#endif // EVG
}; // namespace
