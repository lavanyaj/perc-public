//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __ROBUSTFRUGAL_SWITCH_H
#define __ROBUSTFRUGAL_SWITCH_H

#include <assert.h>
#include <omnetpp.h>
#include "util.h"
#include "GenericSwitch.h"
#include "robustfrugal_m.h"
#include <map>

namespace rcp {

std::string getString(const RobustFrugalPacket *pkt, int forHop=-1);

/*
 * Per session state at link
 *  flowId
 *  seen = 0 (SYN), 1 (regular control packet), 2 (FIN)
 */
class RobustFrugalSwitchSessionStateSeen : public CustomSwitchSessionState {
public:
    int flowId;
    bool forwardSeen;
    bool reverseSeen;


    RobustFrugalSwitchSessionStateSeen() :
        flowId(-1), forwardSeen(false), reverseSeen(false) {
        }
};

/*
 * Per session state at link
 *  flowId
 *  label = sat (0) or unsat (1)
 *  rate
 */
// TODO(lav): Remove all these!? No per-flow state in switches!!!
class RobustFrugalSwitchSessionStateRates : public CustomSwitchSessionState {
public:
    int flowId;
    int label;
    double bottleneckRate;
    double upstreamBandwidth;
    double downstreamBandwidth;

    static const int LABEL_SAT = 0;
    static const int LABEL_UNSAT = 1;
    static const int LABEL_UNDEF = -1;
    // TODO(lav): duplicate sin Host/ Switch State..
    // check consistent
    static std::string labelStr(int label) {
      if (label == LABEL_SAT) return "SAT";
      else if (label == LABEL_UNSAT) return "UNSAT";
      else if (label == LABEL_UNDEF) return "UNDEF";
      else assert(false);
    }

    RobustFrugalSwitchSessionStateRates(int fid, int label, double rate) :
        flowId(fid), label(label),
        upstreamBandwidth(INFINITE_RATE), downstreamBandwidth(INFINITE_RATE),
        bottleneckRate(INFINITE_RATE) {
    }
    RobustFrugalSwitchSessionStateRates() :
        flowId(-1), label(-1),
        upstreamBandwidth(INFINITE_RATE), downstreamBandwidth(INFINITE_RATE),
        bottleneckRate(INFINITE_RATE) {
        }
};

/*
 * State at link
 *  sum of "SAT" flows
 *  number of "UNSAT" flows
 *  number of "SAT" flows - to allow end hosts to compute AVGSAT
 *  also link capacity available for data
 */

//  struct RobustFrugalSwitchUpdate {
//    int flowId;
//    bool forward;
//    double residualLevel;
//    double xstreamBandwidth;
//    double bottleneckRate;
//    double stampedRate;
//    int satLabel;
//
//    RobustFrugalSwitchUpdate(int flowId, bool forward, double residualLevel,
//		       double xstreamBandwidth,
//		       double bottleneckRate, int satLabel,
//		       double stampedRate) :
//						 flowId(flowId), forward(forward),
//						   residualLevel(residualLevel),
//						   xstreamBandwidth(xstreamBandwidth),
//						   bottleneckRate(bottleneckRate),
//						   satLabel(satLabel), stampedRate(stampedRate) {}
//    std::string str() const {
//      std::stringstream ss;
//      ss << " flow " << flowId;
//      if (forward) ss << " fwd"; else ss << " rev";
//      ss << " (R " << residualLevel << ")";
//      if (forward) ss << " (ub " << xstreamBandwidth << ")";
//	else ss << " (db " << xstreamBandwidth << ")";
//      ss << " (br " << bottleneckRate;
//      ss << " ";
//      if (satLabel == RobustFrugalSwitchSessionStateRates::LABEL_UNDEF)
//	ss << "UNDEF)";
//      else if (satLabel == RobustFrugalSwitchSessionStateRates::LABEL_SAT)
//	ss << "SAT)";
//      else if (satLabel == RobustFrugalSwitchSessionStateRates::LABEL_UNSAT)
//	ss << "UNSAT)";
//      ss << " pkt: " << stampedRate;
//      ss << ".  ";
//      return ss.str();
//    }
//  };

 class RobustFrugalSwitch;

class RobustFrugalLinkState : public CustomLinkState {
    public:
    double sumSat;
    int numSat;     // include numSat in the packet so the end hosts can do sumSat/numSat to determine avgSat
    int numUnsat;

    // TODO(lav): make sure everything's initialized before use.
 RobustFrugalLinkState(std::string idStr,
		       cSimpleModule* modulePtr) :
    CustomLinkState(idStr), modulePtr(modulePtr),
      sumSat(0), numSat(0), numUnsat(0)  {};
    ~RobustFrugalLinkState();

    RobustFrugalPacket *processIngress(RobustFrugalPacket *pkt);
    RobustFrugalPacket *processEgress(RobustFrugalPacket *pkt);
    RobustFrugalPacket *processIngressOrEgress(RobustFrugalPacket *pkt, bool egress);

    void forwardingActionKnown(RobustFrugalPacket *pkt);
    void localComputationKnown(const RobustFrugalPacket *pkt, bool egress);
    cSimpleModule* modulePtr;

};


class RobustFrugalSwitch : public GenericSwitch
{

private:
    typedef std::map<int,RobustFrugalLinkState*> RobustFrugalLinkStateTable; // output queue # -> link state
    RobustFrugalLinkStateTable cltable;
protected:
    virtual void initialize();
    virtual void finish();

    virtual cMessage* protocolSpecificProcessIngress(int outGateIndex, cMessage* msg);
    virtual cMessage* protocolSpecificProcessEgress(int outGateIndex, cMessage* msg);

public:
    RobustFrugalSwitch();
    ~RobustFrugalSwitch();
};

}; // namespace

#endif // __ROBUSTFRUGAL_SWITCH_H
